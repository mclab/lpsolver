/*
 * Copyright (C) 2016 Vadim Alimguzhin
 *
 * This file is part of LPSOLVER open source library.
 *
 * LPSOLVER library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * LPSOLVER library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdarg.h>
#include <assert.h>

#include <lpsolver.h>

#include "glpk_macro.h"
#include "lp_solve_macro.h"
#include "cplex_macro.h"

#include "memory.h"
#include "solver.h"
#include "unused.h"

struct LPSolverProblem {
  LPSolver *solver;
  union {
    GLPK_PROBLEM_DEF
    LP_SOLVE_PROBLEM_DEF
    CPLEX_PROBLEM_DEF
  } backend;
};

LPSolverProblem *
lpsolver_problem_new(LPSolver *solver, char const *name, int num_rows, int num_cols) {
  assert(num_rows >= 0);
  assert(num_cols >= 0);
  LPSolverProblem *problem;
  MALLOC(problem, 1, LPSolverProblem);
  problem->solver = solver;
  switch (solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_NEW(problem, name, num_rows, num_cols);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_NEW(problem, name, num_rows, num_cols);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_NEW(problem, name, num_rows, num_cols);
      break;
  }
  return problem;
}

LPSolverProblem *
lpsolver_problem_new_from_file(LPSolver *solver, char const *filename) {
  LPSolverProblem *problem;
  MALLOC(problem, 1, LPSolverProblem);
  problem->solver = solver;
  switch (solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_NEW_FROM_FILE(problem, filename);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_NEW_FROM_FILE(problem, filename);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_NEW_FROM_FILE(problem, filename);
      break;
  }
  return problem;
}

void
lpsolver_problem_destroy(LPSolverProblem **problem_ptr) {
  LPSolverProblem *problem = *problem_ptr;
  switch (problem->solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_DESTROY(problem);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_DESTROY(problem);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_DESTROY(problem);
      break;
  }
  FREE(*problem_ptr);
}

void
lpsolver_problem_write(LPSolverProblem const *problem, char const *filename) {
  switch (problem->solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_WRITE(problem, filename);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_WRITE(problem, filename);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_WRITE(problem, filename);
      break;
  }
}

int
lpsolver_problem_get_num_rows(LPSolverProblem const *problem) {
  int num_rows = 0;
  switch (problem->solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_GET_NUM_ROWS(problem, num_rows);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_GET_NUM_ROWS(problem, num_rows);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_GET_NUM_ROWS(problem, num_rows);
      break;
  }
  return num_rows;
}

int
lpsolver_problem_get_num_cols(LPSolverProblem const * problem) {
  int num_cols = 0;
  switch (problem->solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_GET_NUM_COLS(problem, num_cols);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_GET_NUM_COLS(problem, num_cols);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_GET_NUM_COLS(problem, num_cols);
      break;
  }
  return num_cols;
}

LPSolverColType
lpsolver_problem_get_col_type(LPSolverProblem const *problem, int j) {
  assert(j >= 0);
  LPSolverColType col_type;
  switch (problem->solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_GET_COL_TYPE(problem, j, col_type);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_GET_COL_TYPE(problem, j, col_type);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_GET_COL_TYPE(problem, j, col_type);
      break;
  }
  return col_type;
}

LPSolverColBoundsType
lpsolver_problem_get_col_bounds_type(LPSolverProblem const *problem, int j) {
  assert(j >= 0);
  LPSolverColBoundsType col_bounds_type;
  switch (problem->solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_GET_COL_BOUNDS_TYPE(problem, j, col_bounds_type);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_GET_COL_BOUNDS_TYPE(problem, j, col_bounds_type);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_GET_COL_BOUNDS_TYPE(problem, j, col_bounds_type);
      break;
  }
  return col_bounds_type;
}

void
lpsolver_problem_set_col_type(LPSolverProblem *problem, int j, LPSolverColType col_type) {
  assert(j >= 0);
  switch (problem->solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_SET_COL_TYPE(problem, j, col_type);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_SET_COL_TYPE(problem, j, col_type);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_SET_COL_TYPE(problem, j, col_type);
      break;
  }
}

double
lpsolver_problem_get_col_lb(LPSolverProblem const *problem, int j) {
  assert(j >= 0);
  double lb = 0;
  switch (problem->solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_GET_COL_LB(problem, j, lb);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_GET_COL_LB(problem, j, lb);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_GET_COL_LB(problem, j, lb);
      break;
  }
  return lb;
}

double
lpsolver_problem_get_col_ub(LPSolverProblem const *problem, int j) {
  assert(j >= 0);
  double ub = 0;
  switch (problem->solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_GET_COL_UB(problem, j, ub);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_GET_COL_UB(problem, j, ub);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_GET_COL_UB(problem, j, ub);
      break;
  }
  return ub;
}

void
lpsolver_problem_set_col_bounds(LPSolverProblem *problem, int j, LPSolverColBoundsType bounds_type, double lb, double ub) {
  assert(j >= 0);
  switch (problem->solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_SET_COL_BOUNDS(problem, j, bounds_type, lb, ub);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_SET_COL_BOUNDS(problem, j, bounds_type, lb, ub);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_SET_COL_BOUNDS(problem, j, bounds_type, lb, ub);
      break;
  }
}

char *
lpsolver_problem_get_col_name(LPSolverProblem *problem, int j) {
  assert(j >= 0);
  char *name = NULL;
  switch (problem->solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_GET_COL_NAME(problem, j, name);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_GET_COL_NAME(problem, j, name);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_GET_COL_NAME(problem, j, name);
      break;
  }
  return name;
}

void
lpsolver_problem_set_col_name(LPSolverProblem *problem, int j, char const *name_fmt, ...) {
  assert(j >= 0);
  char *name;
  va_list args;
  va_start(args, name_fmt);
  vasprintf(&name, name_fmt, args);
  va_end(args);
  switch (problem->solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_SET_COL_NAME(problem, j, name);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_SET_COL_NAME(problem, j, name);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_SET_COL_NAME(problem, j, name);
      break;
  }
  FREE(name);
}

int
lpsolver_problem_get_col_idx(LPSolverProblem const *problem, char const *name_fmt, ...) {
  char *name;
  va_list args;
  va_start(args, name_fmt);
  vasprintf(&name, name_fmt, args);
  va_end(args);
  int idx = 0;
  switch (problem->solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_GET_COL_IDX(problem, name, idx);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_GET_COL_IDX(problem, name, idx);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_GET_COL_IDX(problem, name, idx);
      break;
  }
  FREE(name);
  return idx;
}

int
lpsolver_problem_add_col(LPSolverProblem *problem, LPSolverColType col_type, LPSolverColBoundsType bounds_type, double lb, double ub, char const *name_fmt, ...) {
  char *name = NULL;
  if (name_fmt != NULL) {
    va_list args;
    va_start(args, name_fmt);
    vasprintf(&name, name_fmt, args);
    va_end(args);
  }
  int idx = 0;
  switch (problem->solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_ADD_COL(problem, idx);
      GLPK_PROBLEM_SET_COL_TYPE(problem, idx, col_type);
      GLPK_PROBLEM_SET_COL_BOUNDS(problem, idx, bounds_type, lb, ub);
      if (name != NULL) {
        GLPK_PROBLEM_SET_COL_NAME(problem, idx, name);
      }
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_ADD_COL(problem, idx);
      LP_SOLVE_PROBLEM_SET_COL_TYPE(problem, idx, col_type);
      LP_SOLVE_PROBLEM_SET_COL_BOUNDS(problem, idx, bounds_type, lb, ub);
      if (name != NULL) {
        LP_SOLVE_PROBLEM_SET_COL_NAME(problem, idx, name);
      }
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_ADD_COL(problem, idx);
      CPLEX_PROBLEM_SET_COL_TYPE(problem, idx, col_type);
      CPLEX_PROBLEM_SET_COL_BOUNDS(problem, idx, bounds_type, lb, ub);
      if (name != NULL) {
        CPLEX_PROBLEM_SET_COL_NAME(problem, idx, name);
      }
      break;
  }
  FREE(name);
  return idx;
}

double
lpsolver_problem_get_row_coeff(LPSolverProblem const *problem, int i, int j) {
  assert(i >= 0);
  assert(j >= 0);
  double coeff = 0;
  switch (problem->solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_GET_ROW_COEFF(problem, i, j, coeff);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_GET_ROW_COEFF(problem, i, j, coeff);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_GET_ROW_COEFF(problem, i, j, coeff);
      break;
  }
  return coeff;
}

void
lpsolver_problem_set_row_coeff(LPSolverProblem *problem, int i, int j, double coeff) {
  assert(i >= 0);
  assert(j >= 0);
  switch (problem->solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_SET_ROW_COEFF(problem, i, j, coeff);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_SET_ROW_COEFF(problem, i, j, coeff);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_SET_ROW_COEFF(problem, i, j, coeff);
      break;
  }
}

LPSolverRowBoundsType
lpsolver_problem_get_row_bounds_type(LPSolverProblem const * problem, int i) {
  assert(i >= 0);
  LPSolverRowBoundsType bounds_type;
  switch (problem->solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_GET_ROW_BOUNDS_TYPE(problem, i, bounds_type);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_GET_ROW_BOUNDS_TYPE(problem, i, bounds_type);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_GET_ROW_BOUNDS_TYPE(problem, i, bounds_type);
      break;
  }
  return bounds_type;
}

double
lpsolver_problem_get_row_lb(LPSolverProblem const *problem, int i) {
  assert(i >= 0);
  double lb = 0;
  switch (problem->solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_GET_ROW_LB(problem, i, lb);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_GET_ROW_LB(problem, i, lb);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_GET_ROW_LB(problem, i, lb);
      break;
  }
  return lb;
}

double
lpsolver_problem_get_row_ub(LPSolverProblem const *problem, int i) {
  assert(i >= 0);
  double ub = 0;
  switch (problem->solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_GET_ROW_UB(problem, i, ub);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_GET_ROW_UB(problem, i, ub);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_GET_ROW_UB(problem, i, ub);
      break;
  }
  return ub;
}

void
lpsolver_problem_set_row_bounds(LPSolverProblem *problem, int i, LPSolverRowBoundsType bounds_type, double lb, double ub) {
  assert(i >= 0);
  switch (problem->solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_SET_ROW_BOUNDS(problem, i, bounds_type, lb, ub);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_SET_ROW_BOUNDS(problem, i, bounds_type, lb, ub);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_SET_ROW_BOUNDS(problem, i, bounds_type, lb, ub);
      break;
  }
}

char *
lpsolver_problem_get_row_name(LPSolverProblem const *problem, int i) {
  char *name = NULL;
  switch (problem->solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_GET_ROW_NAME(problem, i, name);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_GET_ROW_NAME(problem, i, name);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_GET_ROW_NAME(problem, i, name);
      break;
  }
  return name;
}

void
lpsolver_problem_set_row_name(LPSolverProblem *problem, int i, char const *name_fmt, ...) {
  assert(i >= 0);
  char *name;
  va_list args;
  va_start(args, name_fmt);
  vasprintf(&name, name_fmt, args);
  va_end(args);
  switch (problem->solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_SET_ROW_NAME(problem, i, name);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_SET_ROW_NAME(problem, i, name);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_SET_ROW_NAME(problem, i, name);
      break;
  }
  FREE(name);
}

int
lpsolver_problem_get_row_idx(LPSolverProblem const *problem, char const *name_fmt, ...) {
  char *name;
  va_list args;
  va_start(args, name_fmt);
  vasprintf(&name, name_fmt, args);
  va_end(args);
  int idx = 0;
  switch (problem->solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_GET_ROW_IDX(problem, name, idx);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_GET_ROW_IDX(problem, name, idx);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_GET_ROW_IDX(problem, name, idx);
      break;
  }
  FREE(name);
  return idx;
}

int
lpsolver_problem_add_row(LPSolverProblem *problem, LPSolverRowBoundsType bounds_type, double lb, double ub, char const *name_fmt, ...) {
  char *name = NULL;
  if (name_fmt != NULL) {
    va_list args;
    va_start(args, name_fmt);
    vasprintf(&name, name_fmt, args);
    va_end(args);
  }
  int idx = 0;
  switch (problem->solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_ADD_ROW(problem, idx);
      GLPK_PROBLEM_SET_ROW_BOUNDS(problem, idx, bounds_type, lb, ub);
      if (name != NULL) {
        GLPK_PROBLEM_SET_ROW_NAME(problem, idx, name);
      }
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_ADD_ROW(problem, idx);
      LP_SOLVE_PROBLEM_SET_ROW_BOUNDS(problem, idx, bounds_type, lb, ub);
      if (name != NULL) {
        LP_SOLVE_PROBLEM_SET_ROW_NAME(problem, idx, name);
      }
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_ADD_ROW(problem, idx);
      CPLEX_PROBLEM_SET_ROW_BOUNDS(problem, idx, bounds_type, lb, ub);
      if (name != NULL) {
        CPLEX_PROBLEM_SET_ROW_NAME(problem, idx, name);
      }
      break;
  }
  FREE(name);
  return idx;
}

void
lpsolver_problem_del_row(LPSolverProblem *problem, int i) {
  switch (problem->solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_DEL_ROW(problem, i);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_DEL_ROW(problem, i);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_DEL_ROW(problem, i);
      break;
  }

}

LPSolverObjDir
lpsolver_problem_get_obj_dir(LPSolverProblem const *problem) {
  LPSolverObjDir dir;
  switch (problem->solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_GET_OBJ_DIR(problem, dir);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_GET_OBJ_DIR(problem, dir);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_GET_OBJ_DIR(problem, dir);
      break;
  }
  return dir;
}

void
lpsolver_problem_set_obj_dir(LPSolverProblem *problem, LPSolverObjDir dir) {
  switch (problem->solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_SET_OBJ_DIR(problem, dir);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_SET_OBJ_DIR(problem, dir);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_SET_OBJ_DIR(problem, dir);
      break;
  }
}

double
lpsolver_problem_get_obj_coeff(LPSolverProblem const *problem, int j) {
  assert(j >= 0);
  double coeff = 0;
  switch (problem->solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_GET_OBJ_COEFF(problem, j, coeff);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_GET_OBJ_COEFF(problem, j, coeff);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_GET_OBJ_COEFF(problem, j, coeff);
      break;
  }
  return coeff;
}

void
lpsolver_problem_set_obj_coeff(LPSolverProblem * problem, int j, double coeff) {
  assert(j >= 0);
  switch (problem->solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_SET_OBJ_COEFF(problem, j, coeff);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_SET_OBJ_COEFF(problem, j, coeff);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_SET_OBJ_COEFF(problem, j, coeff);
      break;
  }
}

double
lpsolver_problem_get_obj_offset(LPSolverProblem const *problem) {
  double offset = 0;
  switch (problem->solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_GET_OBJ_OFFSET(problem, offset);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_GET_OBJ_OFFSET(problem, offset);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_GET_OBJ_OFFSET(problem, offset);
      break;
  }
  return offset;
}

void
lpsolver_problem_set_obj_offset(LPSolverProblem *problem, UNUSED(double offset)) {
  switch (problem->solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_SET_OBJ_OFFSET(problem, offset);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_SET_OBJ_OFFSET(problem, offset);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_SET_OBJ_OFFSET(problem, offset);
      break;
  }
}

double
lpsolver_problem_get_obj_coeff_quad(LPSolverProblem const *problem, UNUSED(int j1), UNUSED(int j2)) {
  assert(j1 >= 0);
  assert(j2 >= 0);
  double coeff = 0;
  switch (problem->solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_GET_OBJ_COEFF_QUAD(problem, j1, j2, coeff);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_GET_OBJ_COEFF_QUAD(problem, j1, j2, coeff);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_GET_OBJ_COEFF_QUAD(problem, j1, j2, coeff);
      break;
  }
  return coeff;
}

void
lpsolver_problem_set_obj_coeff_quad(LPSolverProblem *problem, UNUSED(int j1), UNUSED(int j2), UNUSED(double coeff)) {
  assert(j1 >= 0);
  assert(j2 >= 0);
  switch (problem->solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_SET_OBJ_COEFF_QUAD(problem, j1, j2, coeff);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_SET_OBJ_COEFF_QUAD(problem, j1, j2, coeff);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_SET_OBJ_COEFF_QUAD(problem, j1, j2, coeff);
      break;
  }
}

bool
lpsolver_problem_optimize(LPSolverProblem *problem) {
  bool res = false;
  switch (problem->solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_OPTIMIZE(problem, res);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_OPTIMIZE(problem, res);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_OPTIMIZE(problem, res);
      break;
  }
  return res;
}

double
lpsolver_problem_get_obj_value(LPSolverProblem const *problem) {
  double value = 0;
  switch (problem->solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_GET_OBJ_VALUE(problem, value);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_GET_OBJ_VALUE(problem, value);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_GET_OBJ_VALUE(problem, value);
      break;
  }
  return value;
}

double
lpsolver_problem_get_col_value(LPSolverProblem const *problem, int j) {
  assert(j >= 0);
  double value = 0;
  switch (problem->solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_PROBLEM_GET_COL_VALUE(problem, j, value);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_PROBLEM_GET_COL_VALUE(problem, j, value);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_PROBLEM_GET_COL_VALUE(problem, j, value);
      break;
  }
  return value;
}
