/*
 * Copyright (C) 2016 Vadim Alimguzhin
 *
 * This file is part of LPSOLVER open source library.
 *
 * LPSOLVER library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * LPSOLVER library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GLPK_MACRO_H_
#define GLPK_MACRO_H_

#ifdef GLPK_FOUND
#include <glpk.h>
#define SOL_BASIC     1  /* basic */
#define SOL_INTEGER   2  /* mixed integer */
#define USE_STD_BASIS 1  /* use standard basis */
#define USE_ADV_BASIS 2  /* use advanced basis */
#define USE_CPX_BASIS 3  /* use Bixby's basis */
#define GLPK_IS_AVAILABLE true
#define GLPK_SOLVER_DEF struct { char placeholder; } glpk;
#define GLPK_SOLVER_NEW(solver)
#define GLPK_SOLVER_DESTROY(solver)
#define GLPK_SOLVER_SET_NUM_THREADS(solver, num) lpsolver_fatalerror(true, "Setting number of threads is not supported by GLPK backend")
#define GLPK_SOLVER_SET_TIME_LIMIT(solver, limit) lpsolver_fatalerror(true, "Setting time limit is not supported by GLPK backend")
#define GLPK_SOLVER_SET_INTEGRALITY_TOLERANCE(solver, tol) lpsolver_fatalerror(true, "Setting integrality tolerance is not supported by GLPK backend")
#define GLPK_PROBLEM_DEF struct { glp_prob *prob; int *ind; double *val; } glpk;
#define GLPK_PROBLEM_NEW(problem, name, num_rows, num_cols)                \
do {                                                                       \
  (problem)->backend.glpk.prob = glp_create_prob();                        \
  CALLOC((problem)->backend.glpk.ind, (size_t) (num_cols) + 1, int);       \
  CALLOC((problem)->backend.glpk.val, (size_t) (num_cols) + 1, double);    \
  glp_set_prob_name((problem)->backend.glpk.prob, (name));                 \
  if ((num_rows) > 0) {                                                    \
    glp_add_rows((problem)->backend.glpk.prob, (num_rows));                \
    for (int i = 0; i < (num_rows); ++i) {                                 \
      glp_set_row_bnds((problem)->backend.glpk.prob, i + 1, GLP_FX, 0, 0); \
    }                                                                      \
  }                                                                        \
  if ((num_cols) > 0) {                                                    \
    glp_add_cols((problem)->backend.glpk.prob, (num_cols));                \
    for (int j = 0; j < (num_cols); ++j) {                                 \
      glp_set_col_bnds((problem)->backend.glpk.prob, j + 1, GLP_LO, 0, 0); \
    }                                                                      \
  }                                                                        \
  glp_create_index((problem)->backend.glpk.prob);                          \
} while(0)
#define GLPK_PROBLEM_NEW_FROM_FILE(problem, filename)                \
do {                                                                 \
  (problem)->backend.glpk.prob = glp_create_prob();                  \
  glp_read_lp((problem)->backend.glpk.prob, NULL, (filename));       \
  int num_cols = glp_get_num_cols((problem)->backend.glpk.prob) + 1; \
  CALLOC((problem)->backend.glpk.ind, (size_t) num_cols, int);       \
  CALLOC((problem)->backend.glpk.val, (size_t) num_cols, double);    \
  glp_create_index((problem)->backend.glpk.prob);                    \
} while(0)
#define GLPK_PROBLEM_DESTROY(problem)            \
do {                                             \
  glp_delete_prob((problem)->backend.glpk.prob); \
  FREE((problem)->backend.glpk.ind);             \
  FREE((problem)->backend.glpk.val);             \
} while(0)
#define GLPK_PROBLEM_WRITE(problem, filename) glp_write_lp((problem)->backend.glpk.prob, NULL, (filename))
#define GLPK_PROBLEM_GET_NUM_ROWS(problem, num_rows) (num_rows) = glp_get_num_rows((problem)->backend.glpk.prob)
#define GLPK_PROBLEM_GET_NUM_COLS(problem, num_cols) (num_cols) = glp_get_num_cols((problem)->backend.glpk.prob)
#define GLPK_PROBLEM_GET_COL_TYPE(problem, j, col_type)               \
do {                                                                  \
  int kind = glp_get_col_kind((problem)->backend.glpk.prob, (j) + 1); \
  switch (kind) {                                                     \
    case GLP_CV:                                                      \
      (col_type) = LPSOLVER_COL_TYPE_C;                               \
      break;                                                          \
    case GLP_IV:                                                      \
      (col_type) = LPSOLVER_COL_TYPE_I;                               \
      break;                                                          \
    case GLP_BV:                                                      \
      (col_type) = LPSOLVER_COL_TYPE_B;                               \
      break;                                                          \
  }                                                                   \
} while(0)
#define GLPK_PROBLEM_GET_COL_BOUNDS_TYPE(problem, j, col_bounds_type) \
do {                                                                  \
  int t = glp_get_col_type((problem)->backend.glpk.prob, (j) + 1);    \
  switch (t) {                                                        \
  case GLP_FR:                                                        \
    (col_bounds_type) = LPSOLVER_COL_BOUNDS_TYPE_FR;                  \
    break;                                                            \
  case GLP_LO:                                                        \
    (col_bounds_type) = LPSOLVER_COL_BOUNDS_TYPE_LB;                  \
    break;                                                            \
  case GLP_UP:                                                        \
    (col_bounds_type) = LPSOLVER_COL_BOUNDS_TYPE_UB;                  \
    break;                                                            \
  case GLP_DB:                                                        \
    (col_bounds_type) = LPSOLVER_COL_BOUNDS_TYPE_DB;                  \
    break;                                                            \
  case GLP_FX:                                                        \
    (col_bounds_type) = LPSOLVER_COL_BOUNDS_TYPE_FX;                  \
    break;                                                            \
  }                                                                   \
} while(0)
#define GLPK_PROBLEM_SET_COL_TYPE(problem, j, col_type)     \
do {                                                        \
  int kind;                                                 \
  switch ((col_type)) {                                     \
    case LPSOLVER_COL_TYPE_C:                               \
      kind = GLP_CV;                                        \
      break;                                                \
    case LPSOLVER_COL_TYPE_I:                               \
      kind = GLP_IV;                                        \
      break;                                                \
    case LPSOLVER_COL_TYPE_B:                               \
      kind = GLP_BV;                                        \
      break;                                                \
  }                                                         \
  glp_set_col_kind((problem)->backend.glpk.prob, (j) + 1, kind); \
} while(0)
#define GLPK_PROBLEM_GET_COL_LB(problem, j, lb) (lb) = glp_get_col_lb((problem)->backend.glpk.prob, (j) + 1)
#define GLPK_PROBLEM_GET_COL_UB(problem, j, lb) (ub) = glp_get_col_ub((problem)->backend.glpk.prob, (j) + 1)
#define GLPK_PROBLEM_SET_COL_BOUNDS(problem, j, bounds_type, lb, ub)         \
do {                                                                         \
  int type;                                                                  \
  switch((bounds_type)) {                                                    \
    case LPSOLVER_COL_BOUNDS_TYPE_FR:                                        \
      type = GLP_FR;                                                         \
      break;                                                                 \
    case LPSOLVER_COL_BOUNDS_TYPE_FX:                                        \
      type = GLP_FX;                                                         \
      break;                                                                 \
    case LPSOLVER_COL_BOUNDS_TYPE_LB:                                        \
      type = GLP_LO;                                                         \
      break;                                                                 \
    case LPSOLVER_COL_BOUNDS_TYPE_UB:                                        \
      type = GLP_UP;                                                         \
      break;                                                                 \
    case LPSOLVER_COL_BOUNDS_TYPE_DB:                                        \
      type = GLP_DB;                                                         \
      break;                                                                 \
  }                                                                          \
  glp_set_col_bnds((problem)->backend.glpk.prob, (j) + 1, type, (lb), (ub)); \
} while(0)
#define GLPK_PROBLEM_GET_COL_NAME(problem, j, name)                       \
do {                                                                      \
  name = strdup(glp_get_col_name((problem)->backend.glpk.prob, (j) + 1)); \
} while (0)
#define GLPK_PROBLEM_SET_COL_NAME(problem, j, name)                \
do {                                                               \
  glp_set_col_name((problem)->backend.glpk.prob, (j) + 1, (name)); \
} while(0)
#define GLPK_PROBLEM_GET_COL_IDX(problem, name, j)            \
do {                                                          \
  j = glp_find_col((problem)->backend.glpk.prob, (name)) - 1; \
} while(0)
#define GLPK_PROBLEM_ADD_COL(problem, j)                          \
do {                                                              \
  GLPK_PROBLEM_GET_NUM_COLS(problem, j);                          \
  glp_add_cols((problem)->backend.glpk.prob, 1);                  \
  REALLOC((problem)->backend.glpk.ind, (size_t) (j) + 2, int);    \
  REALLOC((problem)->backend.glpk.val, (size_t) (j) + 2, double); \
} while(0)
#define GLPK_PROBLEM_GET_ROW_COEFF(problem, i, j, coeff)                      \
do {                                                                          \
  int *ind = (problem)->backend.glpk.ind;                                     \
  double *val = (problem)->backend.glpk.val;                                  \
  int len = glp_get_mat_row((problem)->backend.glpk.prob, (i) + 1, ind, val); \
  (coeff) = 0;                                                                \
  for (int k = 1; k <= len; ++k) {                                            \
    if (ind[k] == (j) + 1) {                                                  \
      (coeff) = val[k];                                                       \
    }                                                                         \
  }                                                                           \
} while(0)
#define GLPK_PROBLEM_SET_ROW_COEFF(problem, i, j, coeff)                      \
do {                                                                          \
  int *ind = (problem)->backend.glpk.ind;                                     \
  double *val = (problem)->backend.glpk.val;                                  \
  int len = glp_get_mat_row((problem)->backend.glpk.prob, (i) + 1, ind, val); \
  bool nonzero = false;                                                       \
  for (int k = 1; k <= len; ++k) {                                            \
    if (ind[k] == (j) + 1) {                                                  \
      val[k] = (coeff);                                                       \
      nonzero = true;                                                         \
    }                                                                         \
  }                                                                           \
  if (!nonzero) {                                                             \
    len += 1;                                                                 \
    ind[len] = (j) + 1;                                                       \
    val[len] = (coeff);                                                       \
  }                                                                           \
  glp_set_mat_row((problem)->backend.glpk.prob, (i) + 1, len, ind, val);      \
} while(0)
#define GLPK_PROBLEM_GET_ROW_BOUNDS_TYPE(problem, i, bounds_type)     \
do {                                                                  \
  int type = glp_get_row_type((problem)->backend.glpk.prob, (i) + 1); \
  assert(type != GLP_FR);                                             \
  switch (type) {                                                     \
    case GLP_FX:                                                      \
      (bounds_type) = LPSOLVER_ROW_BOUNDS_TYPE_FX;                    \
      break;                                                          \
    case GLP_LO:                                                      \
      (bounds_type) = LPSOLVER_ROW_BOUNDS_TYPE_LB;                    \
      break;                                                          \
    case GLP_UP:                                                      \
      (bounds_type) = LPSOLVER_ROW_BOUNDS_TYPE_UB;                    \
      break;                                                          \
    case GLP_DB:                                                      \
      (bounds_type) = LPSOLVER_ROW_BOUNDS_TYPE_DB;                    \
      break;                                                          \
  }                                                                   \
} while(0)
#define GLPK_PROBLEM_GET_ROW_LB(problem, i, lb) (lb) = glp_get_row_lb((problem)->backend.glpk.prob, (i) + 1)
#define GLPK_PROBLEM_GET_ROW_UB(problem, i, ub) (ub) = glp_get_row_ub((problem)->backend.glpk.prob, (i) + 1)
#define GLPK_PROBLEM_SET_ROW_BOUNDS(problem, i, bounds_type, lb, ub)         \
do {                                                                         \
  int type;                                                                  \
  switch ((bounds_type)) {                                                   \
    case LPSOLVER_ROW_BOUNDS_TYPE_FX:                                        \
      type = GLP_FX;                                                         \
      break;                                                                 \
    case LPSOLVER_ROW_BOUNDS_TYPE_LB:                                        \
      type = GLP_LO;                                                         \
      break;                                                                 \
    case LPSOLVER_ROW_BOUNDS_TYPE_UB:                                        \
      type = GLP_UP;                                                         \
      break;                                                                 \
    case LPSOLVER_ROW_BOUNDS_TYPE_DB:                                        \
      type = GLP_DB;                                                         \
      break;                                                                 \
  }                                                                          \
  glp_set_row_bnds((problem)->backend.glpk.prob, (i) + 1, type, (lb), (ub)); \
} while(0)
#define GLPK_PROBLEM_GET_ROW_NAME(problem, i, name)                         \
do {                                                                        \
  (name) = strdup(glp_get_row_name((problem)->backend.glpk.prob, (i) + 1)); \
} while(0)
#define GLPK_PROBLEM_SET_ROW_NAME(problem, i, name)                \
do {                                                               \
  glp_set_row_name((problem)->backend.glpk.prob, (i) + 1, (name)); \
} while(0)
#define GLPK_PROBLEM_GET_ROW_IDX(problem, name, i)            \
do {                                                          \
  i = glp_find_row((problem)->backend.glpk.prob, (name)) - 1; \
} while(0)
#define GLPK_PROBLEM_ADD_ROW(problem, i)         \
do {                                             \
  GLPK_PROBLEM_GET_NUM_ROWS(problem, i);         \
  glp_add_rows((problem)->backend.glpk.prob, 1); \
} while(0)
#define GLPK_PROBLEM_DEL_ROW(problem, i)               \
do {                                                   \
  int const nums[2] = {0, (i) + 1};                    \
  glp_del_rows((problem)->backend.glpk.prob, 1, nums); \
} while(0)
#define GLPK_PROBLEM_GET_OBJ_DIR(problem, dir) (dir) = glp_get_obj_dir((problem)->backend.glpk.prob) == GLP_MIN ? LPSOLVER_OBJ_DIR_MIN : LPSOLVER_OBJ_DIR_MAX
#define GLPK_PROBLEM_SET_OBJ_DIR(problem, dir) glp_set_obj_dir((problem)->backend.glpk.prob, (dir) == LPSOLVER_OBJ_DIR_MIN ? GLP_MIN : GLP_MAX)
#define GLPK_PROBLEM_GET_OBJ_COEFF(problem, j, coeff) (coeff) = glp_get_obj_coef((problem)->backend.glpk.prob, (j) + 1)
#define GLPK_PROBLEM_SET_OBJ_COEFF(problem, j, coeff) glp_set_obj_coef((problem)->backend.glpk.prob, (j) + 1, (coeff))
#define GLPK_PROBLEM_GET_OBJ_OFFSET(problem, coeff) lpsolver_fatalerror(true, "Objective offset is not supported by GLPK backend")
#define GLPK_PROBLEM_SET_OBJ_OFFSET(problem, coeff) lpsolver_fatalerror(true, "Objective offset is not supported by GLPK backend")
#define GLPK_PROBLEM_GET_OBJ_COEFF_QUAD(problem, j1, j2, coeff) lpsolver_fatalerror(true, "Quadratic objective is not supported by GLPK backend")
#define GLPK_PROBLEM_SET_OBJ_COEFF_QUAD(problem, j1, j2, coeff) lpsolver_fatalerror(true, "Quadratic objective is not supported by GLPK backend")
#define GLPK_PROBLEM_OPTIMIZE(problem, res)                                                       \
do {                                                                                              \
  glp_prob *prob = (problem)->backend.glpk.prob;                                                  \
  int MIP_sol;                                                                                    \
  int lres;                                                                                       \
  int solution = SOL_BASIC;                                                                       \
  int crash = USE_ADV_BASIS;                                                                      \
  glp_smcp smcp;                                                                                  \
  glp_init_smcp(&smcp);                                                                           \
  smcp.presolve = GLP_ON;                                                                         \
  glp_iocp iocp;                                                                                  \
  glp_init_iocp(&iocp);                                                                           \
  iocp.presolve = GLP_ON;                                                                         \
  if (glp_get_num_int(prob) > 0)                                                                  \
    solution = SOL_INTEGER;                                                                       \
  if ((solution == SOL_BASIC && !smcp.presolve) || (solution == SOL_INTEGER && !iocp.presolve))   \
    glp_scale_prob(prob, GLP_SF_AUTO);                                                            \
  if ((solution == SOL_BASIC && !smcp.presolve) || (solution == SOL_INTEGER && !iocp.presolve)) { \
    if (crash == USE_STD_BASIS)                                                                   \
      glp_std_basis(prob);                                                                        \
    else if (crash == USE_ADV_BASIS)                                                              \
      glp_adv_basis(prob, 0);                                                                     \
    else if (crash == USE_CPX_BASIS)                                                              \
      glp_cpx_basis(prob);                                                                        \
  }                                                                                               \
  if (solution == SOL_BASIC) {                                                                    \
    lres = -1;                                                                                    \
    lres = glp_simplex(prob, &smcp);                                                              \
    if (lres != 0 && lres != GLP_ENOPFS && lres != GLP_ENODFS) {                                  \
      abort();                                                                                    \
    }                                                                                             \
    MIP_sol = glp_get_status(prob);                                                               \
  }                                                                                               \
  else {                                                                                          \
    lres = -1;                                                                                    \
    if (!iocp.presolve) {                                                                         \
      lres = glp_simplex(prob, &smcp);                                                            \
      if (lres != 0 && lres != GLP_ENOPFS && lres != GLP_ENODFS) {                                \
        abort();                                                                                  \
      }                                                                                           \
    }                                                                                             \
    if (lres != GLP_ENOPFS && lres != GLP_ENODFS) {                                               \
      lres = glp_intopt(prob, &iocp);                                                             \
      if (lres != 0 && lres != GLP_ENOPFS && lres != GLP_ENODFS) {                                \
        abort();                                                                                  \
      }                                                                                           \
      MIP_sol = glp_mip_status(prob);                                                             \
    }                                                                                             \
    else                                                                                          \
      MIP_sol = glp_get_status(prob);                                                             \
  }                                                                                               \
  (res) = (MIP_sol == GLP_OPT? 1 : 0);                                                            \
} while(0)
#define GLPK_PROBLEM_GET_OBJ_VALUE(problem, value)               \
do {                                                             \
  if (glp_get_num_int((problem)->backend.glpk.prob) > 0) {       \
    (value) = glp_mip_obj_val((problem)->backend.glpk.prob); \
  }                                                              \
  else {                                                         \
    (value) = glp_get_obj_val((problem)->backend.glpk.prob);     \
  }                                                              \
} while(0)
#define GLPK_PROBLEM_GET_COL_VALUE(problem, j, value)                  \
do {                                                                   \
  if (glp_get_num_int((problem)->backend.glpk.prob) > 0) {             \
    (value) = glp_mip_col_val((problem)->backend.glpk.prob, (j) + 1);  \
  }                                                                    \
  else {                                                               \
    (value) = glp_get_col_prim((problem)->backend.glpk.prob, (j) + 1); \
  }                                                                    \
} while(0)
#else
#define GLPK_IS_AVAILABLE false
#define GLPK_SOLVER_DEF
#define GLPK_SOLVER_NEW(solver) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_SOLVER_DESTROY(solver) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_SOLVER_SET_NUM_THREADS(solver, num) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_SOLVER_SET_TIME_LIMIT(solver, num) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_SOLVER_SET_INTEGRALITY_TOLERANCE(solver, tol)  assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_DEF
#define GLPK_PROBLEM_NEW(problem, name, num_rows, num_cols) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_NEW_FROM_FILE(problem, filename) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_DESTROY(problem) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_WRITE(problem, filename) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_GET_NUM_ROWS(problem, num_rows) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_GET_NUM_COLS(problem, num_cols) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_GET_COL_TYPE(problem, j, col_type) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_SET_COL_TYPE(problem, j, col_type) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_GET_COL_BOUNDS_TYPE(problem, j, col_bounds_type) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_GET_COL_LB(problem, j, lb) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_GET_COL_UB(problem, j, lb) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_SET_COL_BOUNDS(problem, j, bounds_type, lb, ub) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_GET_COL_NAME(problem, j, name) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_SET_COL_NAME(problem, j, name) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_GET_COL_IDX(problem, name, j) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_ADD_COL(problem, j) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_GET_ROW_COEFF(problem, i, j, coeff) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_SET_ROW_COEFF(problem, i, j, coeff) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_GET_ROW_BOUNDS_TYPE(problem, i, bounds_type) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_GET_ROW_LB(problem, i, lb) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_GET_ROW_UB(problem, i, ub) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_SET_ROW_BOUNDS(problem, i, bounds_type, lb, ub) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_GET_ROW_NAME(problem, i, name) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_SET_ROW_NAME(problem, i, name) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_GET_ROW_IDX(problem, name, i) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_ADD_ROW(problem, i) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_DEL_ROW(problem, i) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_GET_OBJ_DIR(problem, dir) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_SET_OBJ_DIR(problem, dir) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_GET_OBJ_COEFF(problem, j, coeff) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_SET_OBJ_COEFF(problem, j, coeff) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_GET_OBJ_OFFSET(problem, coeff) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_SET_OBJ_OFFSET(problem, coeff) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_GET_OBJ_COEFF_QUAD(problem, j1, j2, coeff) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_SET_OBJ_COEFF_QUAD(problem, j1, j2, coeff) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_OPTIMIZE(problem, res) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_GET_OBJ_VALUE(problem, value) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#define GLPK_PROBLEM_GET_COL_VALUE(problem, j, value) assert(false); lpsolver_fatalerror(true, "GLPK backend is not available")
#endif /* #ifdef GLPK_FOUND */

#endif /* #ifndef GLPK_MACRO_H_ */
