/*
 * Copyright (C) 2016 Vadim Alimguzhin
 *
 * This file is part of LPSOLVER open source library.
 *
 * LPSOLVER library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * LPSOLVER library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LP_SOLVE_MACRO_H_
#define LP_SOLVE_MACRO_H_

#ifdef LPSOLVE_FOUND
#include <float.h>
#include <lp_lib.h>
#define LP_SOLVE_IS_AVAILABLE true
#define LP_SOLVE_SOLVER_DEF struct { char placeholder; } lp_solve;
#define LP_SOLVE_SOLVER_NEW(solver)
#define LP_SOLVE_SOLVER_DESTROY(solver)
#define LP_SOLVE_SOLVER_SET_NUM_THREADS(solver, num) lpsolver_fatalerror(true, "Setting number of threads is not supported by LP_SOLVE backend")
#define LP_SOLVE_SOLVER_SET_TIME_LIMIT(solver, limit) lpsolver_fatalerror(true, "Setting time limit is not supported by LP_SOLVE backend")
#define LP_SOLVE_SOLVER_SET_INTEGRALITY_TOLERANCE(solver, tol) lpsolver_fatalerror(true, "Setting integrality tolerance is not supported by LP_SOLVE backend")
#define LP_SOLVE_PROBLEM_DEF struct { lprec *prob; double *column; double *row; } lp_solve;
#define LP_SOLVE_PROBLEM_NEW(problem, name, num_rows, num_cols)                \
do {                                                                           \
  (problem)->backend.lp_solve.prob = make_lp((num_rows), (num_cols));          \
  set_verbose((problem)->backend.lp_solve.prob, SEVERE);                       \
  set_XLI((problem)->backend.lp_solve.prob, "xli_CPLEX");                      \
  CALLOC((problem)->backend.lp_solve.column, (size_t) (num_rows) + 1, double); \
  if ((num_cols) > 0) {                                                        \
    CALLOC((problem)->backend.lp_solve.row, (size_t) (num_cols), double);      \
  }                                                                            \
  else {                                                                       \
    (problem)->backend.lp_solve.row = NULL;                                    \
  }                                                                            \
} while(0)
#define LP_SOLVE_PROBLEM_NEW_FROM_FILE(problem, filename)                                          \
do {                                                                                               \
  (problem)->backend.lp_solve.prob = read_XLI("xli_CPLEX", (char *) (filename), NULL, "", NORMAL); \
  set_verbose((problem)->backend.lp_solve.prob, SEVERE);                                           \
  set_XLI((problem)->backend.lp_solve.prob, "xli_CPLEX");                                          \
  int num_rows = get_Norig_rows((problem)->backend.lp_solve.prob);                                 \
  int num_cols = get_Norig_rows((problem)->backend.lp_solve.prob);                                 \
  CALLOC((problem)->backend.lp_solve.column, (size_t) num_rows + 1, double);                       \
  if (num_cols > 0) {                                                                              \
    CALLOC((problem)->backend.lp_solve.row, (size_t) num_cols, double);                            \
  }                                                                                                \
  else {                                                                                           \
    (problem)->backend.lp_solve.row = NULL;                                                        \
  }                                                                                                \
} while(0)
#define LP_SOLVE_PROBLEM_DESTROY(problem)      \
do {                                           \
  delete_lp((problem)->backend.lp_solve.prob); \
} while(0)
#define LP_SOLVE_PROBLEM_WRITE(problem, filename) write_XLI((problem)->backend.lp_solve.prob, (char *) (filename), "", FALSE)
#define LP_SOLVE_PROBLEM_GET_NUM_ROWS(problem, num_rows) (num_rows) = get_Norig_rows((problem)->backend.lp_solve.prob)
#define LP_SOLVE_PROBLEM_GET_NUM_COLS(problem, num_cols) (num_cols) = get_Norig_columns((problem)->backend.lp_solve.prob)
#define LP_SOLVE_PROBLEM_GET_COL_TYPE(problem, j, var_type)     \
do {                                                            \
  if (is_int((problem)->backend.lp_solve.prob, (j) + 1)) {      \
    if (is_binary((problem)->backend.lp_solve.prob, (j) + 1)) { \
      (var_type) = LPSOLVER_COL_TYPE_B;                         \
    }                                                           \
    else {                                                      \
      (var_type) = LPSOLVER_COL_TYPE_I;                         \
    }                                                           \
  }                                                             \
  else {                                                        \
    (var_type) = LPSOLVER_COL_TYPE_C;                           \
  }                                                             \
} while(0)
#define LP_SOLVE_PROBLEM_GET_COL_BOUNDS_TYPE(problem, j, col_bounds_type) \
do {                                                                      \
  double inf = get_infinite((problem)->backend.lp_solve.prob);            \
  double lb = get_lowbo((problem)->backend.lp_solve.prob, (j) + 1);       \
  double ub = get_upbo((problem)->backend.lp_solve.prob, (j) + 1);        \
  if (lb <= -inf) {                                                       \
    if (ub >= inf) {                                                      \
      (col_bounds_type) = LPSOLVER_COL_BOUNDS_TYPE_FR;                    \
    }                                                                     \
    else {                                                                \
      (col_bounds_type) = LPSOLVER_COL_BOUNDS_TYPE_UB;                    \
    }                                                                     \
  }                                                                       \
  else {                                                                  \
    if (ub >= inf) {                                                      \
      (col_bounds_type) = LPSOLVER_COL_BOUNDS_TYPE_LB;                    \
    }                                                                     \
    else {                                                                \
      if (memcmp(&lb, &ub, sizeof(double)) == 0) {                        \
        (col_bounds_type) = LPSOLVER_COL_BOUNDS_TYPE_FX;                  \
      }                                                                   \
      else {                                                              \
        (col_bounds_type) = LPSOLVER_COL_BOUNDS_TYPE_DB;                  \
      }                                                                   \
    }                                                                     \
  }                                                                       \
} while(0)
#define LP_SOLVE_PROBLEM_SET_COL_TYPE(problem, j, var_type)     \
do {                                                            \
  switch ((var_type)) {                                         \
    case LPSOLVER_COL_TYPE_C:                                   \
      set_int((problem)->backend.lp_solve.prob, (j) + 1, 0);    \
      break;                                                    \
    case LPSOLVER_COL_TYPE_I:                                   \
      set_int((problem)->backend.lp_solve.prob, (j) + 1, 1);    \
      break;                                                    \
    case LPSOLVER_COL_TYPE_B:                                   \
      set_binary((problem)->backend.lp_solve.prob, (j) + 1, 1); \
      break;                                                    \
  }                                                             \
} while(0)
#define LP_SOLVE_PROBLEM_GET_COL_LB(problem, j, lb)            \
do {                                                           \
  (lb) = get_lowbo((problem)->backend.lp_solve.prob, (j) + 1); \
  double inf = get_infinite((problem)->backend.lp_solve.prob); \
  if ((lb) <= -inf) {                                          \
    (lb) = -DBL_MAX;                                           \
  }                                                            \
} while(0)
#define LP_SOLVE_PROBLEM_GET_COL_UB(problem, j, ub)            \
do {                                                           \
  (ub) = get_upbo((problem)->backend.lp_solve.prob, (j) + 1);  \
  double inf = get_infinite((problem)->backend.lp_solve.prob); \
  if ((ub) >= inf) {                                           \
    (ub) = DBL_MAX;                                            \
  }                                                            \
} while(0)
#define LP_SOLVE_PROBLEM_SET_COL_BOUNDS(problem, j, bounds_type, lb, ub) \
do {                                                                     \
  double inf = get_infinite((problem)->backend.lp_solve.prob);           \
  double llb, lub;                                                       \
  switch((bounds_type)) {                                                \
    case LPSOLVER_COL_BOUNDS_TYPE_FR:                                    \
      llb = -inf;                                                        \
      lub = inf;                                                         \
      break;                                                             \
    case LPSOLVER_COL_BOUNDS_TYPE_FX:                                    \
      llb = lub = (lb);                                                  \
      break;                                                             \
    case LPSOLVER_COL_BOUNDS_TYPE_LB:                                    \
      llb = (lb);                                                        \
      lub = get_infinite((problem)->backend.lp_solve.prob);              \
      break;                                                             \
    case LPSOLVER_COL_BOUNDS_TYPE_UB:                                    \
      llb = -get_infinite((problem)->backend.lp_solve.prob);             \
      lub = (ub);                                                        \
      break;                                                             \
    case LPSOLVER_COL_BOUNDS_TYPE_DB:                                    \
      llb = (lb);                                                        \
      lub = (ub);                                                        \
      break;                                                             \
  }                                                                      \
  set_bounds((problem)->backend.lp_solve.prob, (j) + 1, llb, lub);       \
} while(0)
#define LP_SOLVE_PROBLEM_GET_COL_NAME(problem, j, name)                     \
do {                                                                        \
  (name) = strdup(get_col_name((problem)->backend.lp_solve.prob, (j) + 1)); \
} while(0)
#define LP_SOLVE_PROBLEM_SET_COL_NAME(problem, j, name)            \
do {                                                               \
  set_col_name((problem)->backend.lp_solve.prob, (j) + 1, (name)); \
} while(0)
#define LP_SOLVE_PROBLEM_GET_COL_IDX(problem, name, j)                  \
do {                                                                    \
  (j) = get_nameindex((problem)->backend.lp_solve.prob, (name), false); \
  if ((j) > 0) {                                                        \
    --(j);                                                              \
  }                                                                     \
} while(0)
#define LP_SOLVE_PROBLEM_ADD_COL(problem, j)                                        \
do {                                                                                \
  LP_SOLVE_PROBLEM_GET_NUM_COLS(problem, j);                                        \
  add_column((problem)->backend.lp_solve.prob, (problem)->backend.lp_solve.column); \
  REALLOC((problem)->backend.lp_solve.row, (size_t) (j) + 1, double);               \
  (problem)->backend.lp_solve.row[(j)] = 0;                                         \
} while(0)
#define LP_SOLVE_PROBLEM_GET_ROW_COEFF(problem, i, j, coeff) (coeff) = get_mat((problem)->backend.lp_solve.prob, (i) + 1, (j) + 1)
#define LP_SOLVE_PROBLEM_SET_ROW_COEFF(problem, i, j, coeff) set_mat((problem)->backend.lp_solve.prob, (i) + 1, (j) + 1, coeff)
#define LP_SOLVE_PROBLEM_GET_ROW_BOUNDS_TYPE(problem, i, bounds_type)                                                  \
do {                                                                                                                   \
  int type = get_constr_type((problem)->backend.lp_solve.prob, (i) + 1);                                               \
  bool with_range;                                                                                                     \
  with_range = is_infinite((problem)->backend.lp_solve.prob, get_rh_range((problem)->backend.lp_solve.prob, (i) + 1)); \
  switch (type) {                                                                                                      \
    case LE:                                                                                                           \
      (bounds_type) = with_range ? LPSOLVER_ROW_BOUNDS_TYPE_DB : LPSOLVER_ROW_BOUNDS_TYPE_LB;                          \
      break;                                                                                                           \
    case GE:                                                                                                           \
      (bounds_type) = with_range ? LPSOLVER_ROW_BOUNDS_TYPE_DB : LPSOLVER_ROW_BOUNDS_TYPE_UB;                          \
      break;                                                                                                           \
    case EQ:                                                                                                           \
      (bounds_type) = LPSOLVER_ROW_BOUNDS_TYPE_FX;                                                                     \
      break;                                                                                                           \
    default:                                                                                                           \
      assert(false);                                                                                                   \
      lpsolver_fatalerror(true, "Must never happen");                                                                  \
      break;                                                                                                           \
  }                                                                                                                    \
} while(0)
#define LP_SOLVE_PROBLEM_GET_ROW_LB(problem, i, lb)                         \
do {                                                                        \
  int type = get_constr_type((problem)->backend.lp_solve.prob, (i) + 1);    \
  double rh = get_rh((problem)->backend.lp_solve.prob, (i) + 1);            \
  if ((type == GE) || (type == EQ)) {                                       \
    (lb) = rh;                                                              \
  }                                                                         \
  else if ((type == LE)) {                                                  \
    double range = get_rh_range((problem)->backend.lp_solve.prob, (i) + 1); \
    if (is_infinite((problem)->backend.lp_solve.prob, range)) {             \
      (lb) = -DBL_MAX;                                                      \
    }                                                                       \
    else {                                                                  \
      (lb) = rh - range;                                                    \
    }                                                                       \
  }                                                                         \
  else {                                                                    \
    assert(false);                                                          \
  }                                                                         \
} while(0)
#define LP_SOLVE_PROBLEM_GET_ROW_UB(problem, i, ub)                         \
do {                                                                        \
  int type = get_constr_type((problem)->backend.lp_solve.prob, (i) + 1);    \
  double rh = get_rh((problem)->backend.lp_solve.prob, (i) + 1);            \
  if ((type == LE) || (type == EQ)) {                                       \
    (ub) = rh;                                                              \
  }                                                                         \
  else if ((type == GE)) {                                                  \
    double range = get_rh_range((problem)->backend.lp_solve.prob, (i) + 1); \
    if (is_infinite((problem)->backend.lp_solve.prob, range)) {             \
      (ub) = DBL_MAX;                                                       \
    }                                                                       \
    else {                                                                  \
      (ub) = rh + range;                                                    \
    }                                                                       \
  }                                                                         \
  else {                                                                    \
    assert(false);                                                          \
  }                                                                         \
} while(0)
#define LP_SOLVE_PROBLEM_SET_ROW_BOUNDS(problem, i, bounds_type, lb, ub) \
do {                                                                     \
  int type;                                                              \
  double rh;                                                             \
  double range;                                                          \
  switch ((bounds_type)) {                                               \
    case LPSOLVER_ROW_BOUNDS_TYPE_FX:                                    \
      type = EQ;                                                         \
      rh = ((lb));                                                       \
      range = get_infinite((problem)->backend.lp_solve.prob);            \
      break;                                                             \
    case LPSOLVER_ROW_BOUNDS_TYPE_LB:                                    \
      type = GE;                                                         \
      rh = ((lb));                                                       \
      range = get_infinite((problem)->backend.lp_solve.prob);            \
      break;                                                             \
    case LPSOLVER_ROW_BOUNDS_TYPE_UB:                                    \
      type = LE;                                                         \
      rh = ((ub));                                                       \
      range = get_infinite((problem)->backend.lp_solve.prob);            \
      break;                                                             \
    case LPSOLVER_ROW_BOUNDS_TYPE_DB:                                    \
      type = LE;                                                         \
      rh = ((lb));                                                       \
      range = ((ub)) - ((lb));                                           \
      break;                                                             \
  }                                                                      \
  set_constr_type((problem)->backend.lp_solve.prob, (i) + 1, type);      \
  set_rh((problem)->backend.lp_solve.prob, (i) + 1, rh);                 \
  set_rh_range((problem)->backend.lp_solve.prob, (i) + 1, range);        \
} while(0)
#define LP_SOLVE_PROBLEM_GET_ROW_NAME(problem, i, name)                     \
do {                                                                        \
  (name) = strdup(get_row_name((problem)->backend.lp_solve.prob, (i) + 1)); \
} while(0)
#define LP_SOLVE_PROBLEM_SET_ROW_NAME(problem, i, name)            \
do {                                                               \
  set_row_name((problem)->backend.lp_solve.prob, (i) + 1, (name)); \
} while(0)
#define LP_SOLVE_PROBLEM_GET_ROW_IDX(problem, name, i)                 \
do {                                                                   \
  (i) = get_nameindex((problem)->backend.lp_solve.prob, (name), true); \
  if ((i) > 0) {                                                       \
    --(i);                                                             \
  }                                                                    \
} while(0)
#define LP_SOLVE_PROBLEM_ADD_ROW(problem, i)                                                \
do {                                                                                        \
  LP_SOLVE_PROBLEM_GET_NUM_ROWS(problem, i);                                                \
  add_constraint((problem)->backend.lp_solve.prob, (problem)->backend.lp_solve.row, EQ, 0); \
  REALLOC((problem)->backend.lp_solve.column, (size_t) (i) + 1, double);                    \
  (problem)->backend.lp_solve.column[(i)] = 0;                                              \
} while(0)
#define LP_SOLVE_PROBLEM_DEL_ROW(problem, i)                                \
do {                                                                        \
  int num_rows = 0;                                                         \
  LP_SOLVE_PROBLEM_GET_NUM_ROWS((problem), num_rows);                       \
  if (num_rows > 1) {                                                       \
    REALLOC((problem)->backend.lp_solve.column, (size_t) num_rows, double); \
  }                                                                         \
  else {                                                                    \
    FREE((problem)->backend.lp_solve.column);                               \
  }                                                                         \
  del_constraint((problem)->backend.lp_solve.prob, (i) + 1);                \
} while(0)
#define LP_SOLVE_PROBLEM_GET_OBJ_DIR(problem, dir) (dir) = is_maxim((problem)->backend.lp_solve.prob) ? LPSOLVER_OBJ_DIR_MAX : LPSOLVER_OBJ_DIR_MIN
#define LP_SOLVE_PROBLEM_SET_OBJ_DIR(problem, dir) set_sense((problem)->backend.lp_solve.prob, (dir) == LPSOLVER_OBJ_DIR_MIN)
#define LP_SOLVE_PROBLEM_GET_OBJ_COEFF(problem, j, coeff) (coeff) = get_mat((problem)->backend.lp_solve.prob, 0, (j) + 1)
#define LP_SOLVE_PROBLEM_SET_OBJ_COEFF(problem, j, coeff) set_mat((problem)->backend.lp_solve.prob, 0, (j) + 1, (coeff))
#define LP_SOLVE_PROBLEM_GET_OBJ_OFFSET(problem, offset) (offset) = get_rh((problem)->backend.lp_solve.prob, 0)
#define LP_SOLVE_PROBLEM_SET_OBJ_OFFSET(problem, offset) set_rh((problem)->backend.lp_solve.prob, 0, (offset))
#define LP_SOLVE_PROBLEM_GET_OBJ_COEFF_QUAD(problem, j1, j2, coeff) lpsolver_fatalerror(true, "Quadratic objective is not supported by LP_SOLVE backend")
#define LP_SOLVE_PROBLEM_SET_OBJ_COEFF_QUAD(problem, j1, j2, coeff) lpsolver_fatalerror(true, "Quadratic objective is not supported by LP_SOLVE backend")
#define LP_SOLVE_PROBLEM_OPTIMIZE(problem, res)                                                                                                         \
do {                                                                                                                                                    \
  set_presolve((problem)->backend.lp_solve.prob, PRESOLVE_ROWS | PRESOLVE_COLS | PRESOLVE_LINDEP, get_presolveloops((problem)->backend.lp_solve.prob)); \
  int lres = solve((problem)->backend.lp_solve.prob);                                                                                                   \
  (res) = lres == OPTIMAL || lres == PRESOLVED;                                                                                                         \
} while(0)
#define LP_SOLVE_PROBLEM_GET_OBJ_VALUE(problem, value) (value) = get_objective((problem)->backend.lp_solve.prob)
#define LP_SOLVE_PROBLEM_GET_COL_VALUE(problem, j, value) (value) = get_mat((problem)->backend.lp_solve.prob, 0, (j) + 1)
#else
#define LP_SOLVE_IS_AVAILABLE false
#define LP_SOLVE_SOLVER_DEF
#define LP_SOLVE_SOLVER_NEW(solver) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_SOLVER_DESTROY(solver) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_SOLVER_SET_NUM_THREADS(solver, num) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_SOLVER_SET_TIME_LIMIT(solver, limit) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_SOLVER_SET_INTEGRALITY_TOLERANCE(solver, tol) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_DEF
#define LP_SOLVE_PROBLEM_NEW(problem, name, num_rows, num_cols) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_NEW_FROM_FILE(problem, filename) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_DESTROY(problem) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_WRITE(problem, filename) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_GET_NUM_ROWS(problem, num_rows) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_GET_NUM_COLS(problem, num_cols) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_GET_COL_TYPE(problem, j, var_type) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_SET_COL_TYPE(problem, j, var_type) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_GET_COL_BOUNDS_TYPE(problem, j, col_bounds_type) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_GET_COL_LB(problem, j, lb) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_GET_COL_UB(problem, j, lb) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_SET_COL_BOUNDS(problem, j, bounds_type, lb, ub) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_GET_COL_NAME(problem, j, name) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_SET_COL_NAME(problem, j, name) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_GET_COL_IDX(problem, name, j) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_ADD_COL(problem, j) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_GET_ROW_COEFF(problem, i, j, coeff) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_SET_ROW_COEFF(problem, i, j, coeff) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_GET_ROW_BOUNDS_TYPE(problem, i, bounds_type) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_GET_ROW_LB(problem, i, lb) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_GET_ROW_UB(problem, i, ub) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_SET_ROW_BOUNDS(problem, i, bounds_type, lb, ub) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_GET_ROW_NAME(problem, j, name) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_SET_ROW_NAME(problem, j, name) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_GET_ROW_IDX(problem, name, j) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_ADD_ROW(problem, i) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_DEL_ROW(problem, i) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_GET_OBJ_DIR(problem, dir) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_SET_OBJ_DIR(problem, dir) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_GET_OBJ_COEFF(problem, j, coeff) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_SET_OBJ_COEFF(problem, j, coeff) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_GET_OBJ_OFFSET(problem, coeff) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_SET_OBJ_OFFSET(problem, coeff) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_GET_OBJ_COEFF_QUAD(problem, j1, j2, coeff) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_SET_OBJ_COEFF_QUAD(problem, j1, j2, coeff) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_OPTIMIZE(problem, res) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_GET_OBJ_VALUE(problem, value) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#define LP_SOLVE_PROBLEM_GET_COL_VALUE(problem, j, value) assert(false); lpsolver_fatalerror(true, "LP_SOLVE backend is not available")
#endif /* #ifdef LPSOLVE_FOUND */

#endif /* #ifndef LP_SOLVE_MACRO_H_ */
