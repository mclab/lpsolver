/*
 * Copyright (C) 2016 Vadim Alimguzhin
 *
 * This file is part of LPSOLVER open source library.
 *
 * LPSOLVER library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * LPSOLVER library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <assert.h>

#include <lpsolver.h>

#include "glpk_macro.h"
#include "lp_solve_macro.h"
#include "cplex_macro.h"

#include "memory.h"
#include "unused.h"

#include "solver.h"

bool
lpsolver_has_backend(LPSolverBackendType backend_type) {
  switch (backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      return GLPK_IS_AVAILABLE;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      return LP_SOLVE_IS_AVAILABLE;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      return CPLEX_IS_AVAILABLE;
  }
  return false;
}

LPSolver *
lpsolver_new(LPSolverBackendType backend_type) {
  LPSolver *solver = NULL;
  MALLOC(solver, 1, LPSolver);
  solver->backend_type = backend_type;
  switch (backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      assert(GLPK_IS_AVAILABLE);
      lpsolver_fatalerror(!GLPK_IS_AVAILABLE, "GLPK backend is not available");
      GLPK_SOLVER_NEW(solver);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      assert(LP_SOLVE_IS_AVAILABLE);
      lpsolver_fatalerror(!LP_SOLVE_IS_AVAILABLE, "LP_SOLVE backend is not available");
      LP_SOLVE_SOLVER_NEW(solver);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      assert(CPLEX_IS_AVAILABLE);
      lpsolver_fatalerror(!CPLEX_IS_AVAILABLE, "CPLEX backend is not available");
      CPLEX_SOLVER_NEW(solver);
      break;
  }
  return solver;
}

void
lpsolver_destroy(LPSolver **solver_ptr) {
  LPSolver *solver = *solver_ptr;
  switch (solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_SOLVER_DESTROY(solver);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_SOLVER_DESTROY(solver);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_SOLVER_DESTROY(solver);
      break;
  }
  FREE(*solver_ptr);
}

void
lpsolver_set_num_threads(LPSolver *solver, UNUSED(int num)) {
  switch (solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_SOLVER_SET_NUM_THREADS(solver, num);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_SOLVER_SET_NUM_THREADS(solver, num);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_SOLVER_SET_NUM_THREADS(solver, num);
      break;
  }
}

void
lpsolver_set_time_limit(LPSolver *solver, double limit) {
  switch (solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_SOLVER_SET_TIME_LIMIT(solver, limit);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_SOLVER_SET_TIME_LIMIT(solver, limit);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_SOLVER_SET_TIME_LIMIT(solver, limit);
      break;
  }
}

void
lpsolver_set_integrality_tolerance(LPSolver *solver, double tol) {
  switch (solver->backend_type) {
    case LPSOLVER_BACKEND_TYPE_GLPK:
      GLPK_SOLVER_SET_INTEGRALITY_TOLERANCE(solver, tol);
      break;
    case LPSOLVER_BACKEND_TYPE_LP_SOLVE:
      LP_SOLVE_SOLVER_SET_INTEGRALITY_TOLERANCE(solver, tol);
      break;
    case LPSOLVER_BACKEND_TYPE_CPLEX:
      CPLEX_SOLVER_SET_INTEGRALITY_TOLERANCE(solver, tol);
      break;
  }
}
