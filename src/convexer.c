#include <stdlib.h>
#include <stdio.h>
#include <float.h>
#include <stdarg.h>
#include <string.h>

#include <lpsolver.h>

static int
aux(LPSolverProblem *problem, int row_idx, int col_idx, bool negated) {
  double m = 0;
  double b = lpsolver_problem_get_row_ub(problem, row_idx);
  for (int j = 0; j < lpsolver_problem_get_num_cols(problem); ++j) {
    double coeff = lpsolver_problem_get_row_coeff(problem, row_idx, j);
    if (coeff >= 0) {
      double bound = lpsolver_problem_get_col_ub(problem, j);
      m += coeff * bound;
    }
    else {
      double bound = lpsolver_problem_get_col_lb(problem, j);
      m += coeff * bound;
    }
  }
  if (negated) {
    lpsolver_problem_set_row_coeff(problem, row_idx, col_idx, b - m);
  }
  else {
    lpsolver_problem_set_row_coeff(problem, row_idx, col_idx, m - b);
    lpsolver_problem_set_row_bounds(problem, row_idx,
                                    LPSOLVER_ROW_BOUNDS_TYPE_UB, -DBL_MAX, m);
  }
  return 0;
}

int
lpsolver_problem_add_guard(LPSolverProblem *problem, int row_idx,
                           int guard_col_idx, bool negated) {
  int res = 0;
  double coeff =
      lpsolver_problem_get_row_coeff(problem, row_idx, guard_col_idx);
  double zero = 0.0;
  if (memcmp(&zero, &coeff, sizeof(double)) != 0) {
    res = -1;
    goto end;
  }
  if (lpsolver_problem_get_col_type(problem, guard_col_idx) !=
      LPSOLVER_COL_TYPE_B) {
    res = -2;
    goto end;
  }
  for (int j = 0; j < lpsolver_problem_get_num_cols(problem); ++j) {
    double coeff = lpsolver_problem_get_row_coeff(problem, row_idx, j);
    if (memcmp(&coeff, &zero, sizeof(double)) != 0) {
      LPSolverColBoundsType col_bounds_type =
          lpsolver_problem_get_col_bounds_type(problem, j);
      if ((col_bounds_type != LPSOLVER_COL_BOUNDS_TYPE_DB) &&
          (col_bounds_type != LPSOLVER_COL_BOUNDS_TYPE_FX)) {
        res = -3;
        goto end;
      }
    }
  }
  LPSolverRowBoundsType row_bounds_type =
      lpsolver_problem_get_row_bounds_type(problem, row_idx);
  char *row_name = lpsolver_problem_get_row_name(problem, row_idx);
  switch (row_bounds_type) {
  case LPSOLVER_ROW_BOUNDS_TYPE_LB:
    {
      for (int j = 0; j < lpsolver_problem_get_num_cols(problem); ++j) {
        double coeff = lpsolver_problem_get_row_coeff(problem, row_idx, j);
        lpsolver_problem_set_row_coeff(problem, row_idx, j, -1 * coeff);
      }
      double b = lpsolver_problem_get_row_lb(problem, row_idx);
      lpsolver_problem_set_row_bounds(
          problem, row_idx, LPSOLVER_ROW_BOUNDS_TYPE_UB, -DBL_MAX, -1 * b);
      aux(problem, row_idx, guard_col_idx, negated);
    }
    break;
  case LPSOLVER_ROW_BOUNDS_TYPE_UB:
    aux(problem, row_idx, guard_col_idx, negated);
    break;
  case LPSOLVER_ROW_BOUNDS_TYPE_DB:
  case LPSOLVER_ROW_BOUNDS_TYPE_FX:
    {
      double lb = lpsolver_problem_get_row_lb(problem, row_idx);
      double ub = lpsolver_problem_get_row_ub(problem, row_idx);
      int row_idx_ub =
          lpsolver_problem_add_row(problem, LPSOLVER_ROW_BOUNDS_TYPE_UB,
                                   -DBL_MAX, ub, "%s_%s", row_name, "ub");
      int row_idx_lb =
          lpsolver_problem_add_row(problem, LPSOLVER_ROW_BOUNDS_TYPE_UB,
                                   -DBL_MAX, -1 * lb, "%s_%s", row_name, "lb");
      for (int j = 0; j < lpsolver_problem_get_num_cols(problem); ++j) {
        double coeff = lpsolver_problem_get_row_coeff(problem, row_idx, j);
        lpsolver_problem_set_row_coeff(problem, row_idx_ub, j, coeff);
        lpsolver_problem_set_row_coeff(problem, row_idx_lb, j, -1 * coeff);
      }
      aux(problem, row_idx_ub, guard_col_idx, negated);
      aux(problem, row_idx_lb, guard_col_idx, negated);
      lpsolver_problem_del_row(problem, row_idx);
    }
    break;
  }
  free(row_name);
end:
  return res;
}
