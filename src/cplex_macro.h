/*
 * Copyright (C) 2016 Vadim Alimguzhin
 *
 * This file is part of LPSOLVER open source library.
 *
 * LPSOLVER library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * LPSOLVER library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPLEX_MACRO_H_
#define CPLEX_MACRO_H_

#include "error.h"

#ifdef CPLEX_FOUND
#include <ilcplex/cplex.h>
#include <float.h>
#define CPLEX_FATALERROR(solver, status) lpsolver_fatalerror(status != 0, "CPLEX %s", CPXgeterrorstring((solver)->backend.cplex.env, status, (solver)->backend.cplex.message))
#define CPLEX_IS_AVAILABLE true
#define CPLEX_SOLVER_DEF struct { CPXENVptr env; char message[CPXMESSAGEBUFSIZE]; } cplex;
#define CPLEX_SOLVER_NEW(solver)                                             \
do {                                                                         \
  int status;                                                                \
  (solver)->backend.cplex.env = CPXopenCPLEX(&status);                       \
  CPLEX_FATALERROR((solver), status);                                        \
} while(0)
#define CPLEX_SOLVER_DESTROY(solver) CPXcloseCPLEX(&(solver)->backend.cplex.env)
#define CPLEX_SOLVER_SET_NUM_THREADS(solver, num)                               \
do {                                                                            \
  int status;                                                                   \
  status = CPXsetintparam((solver)->backend.cplex.env, CPX_PARAM_THREADS, num); \
  CPLEX_FATALERROR((solver), status);                                           \
} while (0)
#define CPLEX_SOLVER_SET_TIME_LIMIT(solver, limit)                              \
do {                                                                            \
  int status;                                                                   \
  status = CPXsetdblparam((solver)->backend.cplex.env, CPX_PARAM_TILIM, limit); \
  CPLEX_FATALERROR((solver), status);                                           \
} while (0)
#define CPLEX_SOLVER_SET_INTEGRALITY_TOLERANCE(solver, tol)                                       \
do {                                                                                              \
  int status;                                                                                     \
  status = CPXsetdblparam((solver)->backend.cplex.env, CPXPARAM_MIP_Tolerances_Integrality, tol); \
  CPLEX_FATALERROR((solver), status);                                                             \
} while (0)
#define CPLEX_PROBLEM_DEF CPXLPptr cplex;
#define CPLEX_PROBLEM_NEW(problem, name, num_rows, num_cols)                                                                     \
do {                                                                                                                             \
  int status;                                                                                                                    \
  (problem)->backend.cplex = CPXcreateprob((problem)->solver->backend.cplex.env, &status, (name));                               \
  CPLEX_FATALERROR((problem)->solver, status);                                                                                   \
  status = CPXnewrows((problem)->solver->backend.cplex.env, (problem)->backend.cplex, (num_rows), NULL, NULL, NULL, NULL);       \
  CPLEX_FATALERROR((problem)->solver, status);                                                                                   \
  status = CPXnewcols((problem)->solver->backend.cplex.env, (problem)->backend.cplex, (num_cols), NULL, NULL, NULL, NULL, NULL); \
  CPLEX_FATALERROR((problem)->solver, status);                                                                                   \
} while(0)
#define CPLEX_PROBLEM_NEW_FROM_FILE(problem, filename)                                                        \
do {                                                                                                          \
  int status;                                                                                                 \
  (problem)->backend.cplex = CPXcreateprob((problem)->solver->backend.cplex.env, &status, "problem");         \
  CPLEX_FATALERROR((problem)->solver, status);                                                                \
  status = CPXreadcopyprob((problem)->solver->backend.cplex.env, (problem)->backend.cplex, (filename), "LP"); \
  CPLEX_FATALERROR((problem)->solver, status);                                                                \
} while(0)
#define CPLEX_PROBLEM_DESTROY(problem) CPXfreeprob((problem)->solver->backend.cplex.env, &(problem)->backend.cplex)
#define CPLEX_PROBLEM_WRITE(problem, filename)                                                             \
do {                                                                                                       \
  int status;                                                                                              \
  status = CPXwriteprob((problem)->solver->backend.cplex.env, (problem)->backend.cplex, (filename), "LP"); \
  CPLEX_FATALERROR((problem)->solver, status);                                                             \
} while(0)
#define CPLEX_PROBLEM_GET_NUM_ROWS(problem, num_rows) (num_rows) = CPXgetnumrows((problem)->solver->backend.cplex.env, (problem)->backend.cplex)
#define CPLEX_PROBLEM_GET_NUM_COLS(problem, num_cols) (num_cols) = CPXgetnumcols((problem)->solver->backend.cplex.env, (problem)->backend.cplex)
#define CPLEX_PROBLEM_GET_COL_TYPE(problem, j, col_type)                                                  \
do {                                                                                                      \
  int status;                                                                                             \
  char ctype;                                                                                             \
  status = CPXgetctype((problem)->solver->backend.cplex.env, (problem)->backend.cplex, &ctype, (j), (j)); \
  CPLEX_FATALERROR((problem)->solver, status);                                                            \
  switch (ctype) {                                                                                        \
    case CPX_CONTINUOUS:                                                                                  \
      (col_type) = LPSOLVER_COL_TYPE_C;                                                                   \
      break;                                                                                              \
    case CPX_INTEGER:                                                                                     \
      (col_type) = LPSOLVER_COL_TYPE_I;                                                                   \
      break;                                                                                              \
    case CPX_BINARY:                                                                                      \
      (col_type) = LPSOLVER_COL_TYPE_B;                                                                   \
      break;                                                                                              \
  }                                                                                                       \
} while(0)
#define CPLEX_PROBLEM_GET_COL_BOUNDS_TYPE(problem, j, col_bounds_type)                              \
do {                                                                                                \
  int status;                                                                                       \
  double lb = NAN;                                                                                  \
  status = CPXgetlb((problem)->solver->backend.cplex.env, (problem)->backend.cplex, &lb, (j), (j)); \
  CPLEX_FATALERROR((problem)->solver, status);                                                      \
  double ub = NAN;                                                                                  \
  status = CPXgetub((problem)->solver->backend.cplex.env, (problem)->backend.cplex, &ub, (j), (j)); \
  CPLEX_FATALERROR((problem)->solver, status);                                                      \
  if (lb <= -CPX_INFBOUND) {                                                                        \
    if (ub >= CPX_INFBOUND) {                                                                       \
      (col_bounds_type) = LPSOLVER_COL_BOUNDS_TYPE_FR;                                              \
    }                                                                                               \
    else {                                                                                          \
      (col_bounds_type) = LPSOLVER_COL_BOUNDS_TYPE_UB;                                              \
    }                                                                                               \
  }                                                                                                 \
  else {                                                                                            \
    if (ub >= CPX_INFBOUND) {                                                                       \
      (col_bounds_type) = LPSOLVER_COL_BOUNDS_TYPE_LB;                                              \
    }                                                                                               \
    else {                                                                                          \
      if (memcmp(&lb, &ub, sizeof(double)) == 0) {                                                  \
        (col_bounds_type) = LPSOLVER_COL_BOUNDS_TYPE_FX;                                            \
      }                                                                                             \
      else {                                                                                        \
        (col_bounds_type) = LPSOLVER_COL_BOUNDS_TYPE_DB;                                            \
      }                                                                                             \
    }                                                                                               \
  }                                                                                                 \
} while(0)
#define CPLEX_PROBLEM_SET_COL_TYPE(problem, j, col_type)                                                 \
do {                                                                                                     \
  int status;                                                                                            \
  char ctype;                                                                                            \
  switch ((col_type)) {                                                                                  \
    case LPSOLVER_COL_TYPE_C:                                                                            \
      ctype = CPX_CONTINUOUS;                                                                            \
      break;                                                                                             \
    case LPSOLVER_COL_TYPE_I:                                                                            \
      ctype = CPX_INTEGER;                                                                               \
      break;                                                                                             \
    case LPSOLVER_COL_TYPE_B:                                                                            \
      ctype = CPX_BINARY;                                                                                \
      break;                                                                                             \
  }                                                                                                      \
  status = CPXchgctype((problem)->solver->backend.cplex.env, (problem)->backend.cplex, 1, &(j), &ctype); \
  CPLEX_FATALERROR((problem)->solver, status);                                                           \
} while(0)
#define CPLEX_PROBLEM_GET_COL_LB(problem, j, lb)                                                      \
do {                                                                                                  \
  int status;                                                                                         \
  status = CPXgetlb((problem)->solver->backend.cplex.env, (problem)->backend.cplex, &(lb), (j), (j)); \
  CPLEX_FATALERROR((problem)->solver, status);                                                        \
  if ((lb) <= -CPX_INFBOUND) {                                                                        \
    (lb) = -DBL_MAX;                                                                                  \
  }                                                                                                   \
} while(0)
#define CPLEX_PROBLEM_GET_COL_UB(problem, j, ub)                                                      \
do {                                                                                                  \
  int status;                                                                                         \
  status = CPXgetub((problem)->solver->backend.cplex.env, (problem)->backend.cplex, &(ub), (j), (j)); \
  CPLEX_FATALERROR((problem)->solver, status);                                                        \
  if ((ub) >= CPX_INFBOUND) {                                                                         \
    (ub) = DBL_MAX;                                                                                   \
  }                                                                                                   \
} while(0)
#define CPLEX_PROBLEM_SET_COL_BOUNDS(problem, j, bounds_type, lb, ub)                                        \
do {                                                                                                         \
  int status;                                                                                                \
  char lu;                                                                                                   \
  double bd;                                                                                                 \
  switch((bounds_type)) {                                                                                    \
    case LPSOLVER_COL_BOUNDS_TYPE_FR:                                                                        \
      lu = 'L';                                                                                              \
      bd = -CPX_INFBOUND;                                                                                    \
      status = CPXchgbds((problem)->solver->backend.cplex.env, (problem)->backend.cplex, 1, &(j), &lu, &bd); \
      CPLEX_FATALERROR((problem)->solver, status);                                                           \
      lu = 'U';                                                                                              \
      bd = CPX_INFBOUND;                                                                                     \
      status = CPXchgbds((problem)->solver->backend.cplex.env, (problem)->backend.cplex, 1, &(j), &lu, &bd); \
      CPLEX_FATALERROR((problem)->solver, status);                                                           \
      break;                                                                                                 \
    case LPSOLVER_COL_BOUNDS_TYPE_FX:                                                                        \
      lu = 'B';                                                                                              \
      bd = (lb);                                                                                             \
      status = CPXchgbds((problem)->solver->backend.cplex.env, (problem)->backend.cplex, 1, &(j), &lu, &bd); \
      CPLEX_FATALERROR((problem)->solver, status);                                                           \
      break;                                                                                                 \
    case LPSOLVER_COL_BOUNDS_TYPE_LB:                                                                        \
      lu = 'L';                                                                                              \
      bd = (lb);                                                                                             \
      status = CPXchgbds((problem)->solver->backend.cplex.env, (problem)->backend.cplex, 1, &(j), &lu, &bd); \
      CPLEX_FATALERROR((problem)->solver, status);                                                           \
      lu = 'U';                                                                                              \
      bd = CPX_INFBOUND;                                                                                     \
      status = CPXchgbds((problem)->solver->backend.cplex.env, (problem)->backend.cplex, 1, &(j), &lu, &bd); \
      CPLEX_FATALERROR((problem)->solver, status);                                                           \
      break;                                                                                                 \
    case LPSOLVER_COL_BOUNDS_TYPE_UB:                                                                        \
      lu = 'U';                                                                                              \
      bd = (ub);                                                                                             \
      status = CPXchgbds((problem)->solver->backend.cplex.env, (problem)->backend.cplex, 1, &(j), &lu, &bd); \
      CPLEX_FATALERROR((problem)->solver, status);                                                           \
      lu = 'L';                                                                                              \
      bd = -CPX_INFBOUND;                                                                                    \
      status = CPXchgbds((problem)->solver->backend.cplex.env, (problem)->backend.cplex, 1, &(j), &lu, &bd); \
      CPLEX_FATALERROR((problem)->solver, status);                                                           \
      break;                                                                                                 \
    case LPSOLVER_COL_BOUNDS_TYPE_DB:                                                                        \
      lu = 'L';                                                                                              \
      bd = (lb);                                                                                             \
      status = CPXchgbds((problem)->solver->backend.cplex.env, (problem)->backend.cplex, 1, &(j), &lu, &bd); \
      CPLEX_FATALERROR((problem)->solver, status);                                                           \
      lu = 'U';                                                                                              \
      bd = (ub);                                                                                             \
      status = CPXchgbds((problem)->solver->backend.cplex.env, (problem)->backend.cplex, 1, &(j), &lu, &bd); \
      CPLEX_FATALERROR((problem)->solver, status);                                                           \
      break;                                                                                                 \
  }                                                                                                          \
} while(0)
#define CPLEX_PROBLEM_GET_COL_NAME(problem, j, name) \
do { \
  int surplus = 0;                                                                                                              \
  int status = 0;                                                                                                               \
  status = CPXgetcolname((problem)->solver->backend.cplex.env, (problem)->backend.cplex, NULL, NULL, 0, &surplus, j, j);        \
  int sz = -surplus;                                                                                                            \
  char *namestore = malloc((size_t) sz);                                                                                        \
  status = CPXgetcolname((problem)->solver->backend.cplex.env, (problem)->backend.cplex, &name, namestore, sz, &surplus, j, j); \
  CPLEX_FATALERROR((problem)->solver, status);                                                           \
} while(0)
#define CPLEX_PROBLEM_SET_COL_NAME(problem, j, name)                                                         \
do {                                                                                                         \
  int status = CPXchgname((problem)->solver->backend.cplex.env, (problem)->backend.cplex, 'c', (j), (name)); \
  CPLEX_FATALERROR((problem)->solver, status);                                                               \
} while(0)
#define CPLEX_PROBLEM_GET_COL_IDX(problem, name, j)                                                          \
do {                                                                                                         \
  int status = CPXgetcolindex((problem)->solver->backend.cplex.env, (problem)->backend.cplex, (name), &(j)); \
  if (status != 0) {                                                                                         \
    (j) = -1;                                                                                                \
  }                                                                                                          \
} while(0)
#define CPLEX_PROBLEM_ADD_COL(problem, j)                                                                                     \
do {                                                                                                                          \
  CPLEX_PROBLEM_GET_NUM_COLS(problem, j);                                                                                     \
  int status = CPXnewcols((problem)->solver->backend.cplex.env, (problem)->backend.cplex, (1), NULL, NULL, NULL, NULL, NULL); \
  CPLEX_FATALERROR((problem)->solver, status);                                                                                \
} while(0)
#define CPLEX_PROBLEM_GET_ROW_COEFF(problem, i, j, coeff)                                                  \
do {                                                                                                       \
  int status;                                                                                              \
  status = CPXgetcoef((problem)->solver->backend.cplex.env, (problem)->backend.cplex, (i), (j), &(coeff)); \
  CPLEX_FATALERROR((problem)->solver, status);                                                             \
} while(0)
#define CPLEX_PROBLEM_SET_ROW_COEFF(problem, i, j, coeff)                                                 \
do {                                                                                                      \
  int status;                                                                                             \
  status = CPXchgcoef((problem)->solver->backend.cplex.env, (problem)->backend.cplex, (i), (j), (coeff)); \
  CPLEX_FATALERROR((problem)->solver, status);                                                            \
} while(0)
#define CPLEX_PROBLEM_GET_ROW_BOUNDS_TYPE(problem, i, bounds_type)                                        \
do {                                                                                                      \
  int status;                                                                                             \
  char sense;                                                                                             \
  status = CPXgetsense((problem)->solver->backend.cplex.env, (problem)->backend.cplex, &sense, (i), (i)); \
  CPLEX_FATALERROR((problem)->solver, status);                                                            \
  switch (sense) {                                                                                        \
    case 'E':                                                                                             \
      (bounds_type) = LPSOLVER_ROW_BOUNDS_TYPE_FX;                                                        \
      break;                                                                                              \
    case 'L':                                                                                             \
      (bounds_type) = LPSOLVER_ROW_BOUNDS_TYPE_UB;                                                        \
      break;                                                                                              \
    case 'G':                                                                                             \
      (bounds_type) = LPSOLVER_ROW_BOUNDS_TYPE_LB;                                                        \
      break;                                                                                              \
    case 'R':                                                                                             \
      (bounds_type) = LPSOLVER_ROW_BOUNDS_TYPE_DB;                                                        \
      break;                                                                                              \
  }                                                                                                       \
} while(0)
#define CPLEX_PROBLEM_GET_ROW_LB(problem, i, lb)                                                          \
do {                                                                                                      \
  int status;                                                                                             \
  char sense;                                                                                             \
  double rhs;                                                                                             \
  status = CPXgetsense((problem)->solver->backend.cplex.env, (problem)->backend.cplex, &sense, (i), (i)); \
  CPLEX_FATALERROR((problem)->solver, status);                                                            \
  if (sense == 'L') {                                                                                     \
    (lb) = -DBL_MAX;                                                                                      \
  }                                                                                                       \
  else {                                                                                                  \
    status = CPXgetrhs((problem)->solver->backend.cplex.env, (problem)->backend.cplex, &rhs, (i), (i));   \
    CPLEX_FATALERROR((problem)->solver, status);                                                          \
    (lb) = rhs;                                                                                           \
  }                                                                                                       \
} while(0)
#define CPLEX_PROBLEM_GET_ROW_UB(problem, i, ub)                                                                \
do {                                                                                                            \
  int status;                                                                                                   \
  char sense;                                                                                                   \
  double rhs;                                                                                                   \
  double rngval;                                                                                                \
  status = CPXgetsense((problem)->solver->backend.cplex.env, (problem)->backend.cplex, &sense, (i), (i));       \
  CPLEX_FATALERROR((problem)->solver, status);                                                                  \
  if (sense == 'G') {                                                                                           \
    (ub) = DBL_MAX;                                                                                             \
  }                                                                                                             \
  else {                                                                                                        \
    status = CPXgetrhs((problem)->solver->backend.cplex.env, (problem)->backend.cplex, &rhs, (i), (i));         \
    CPLEX_FATALERROR((problem)->solver, status);                                                                \
    if (sense == 'R') {                                                                                         \
      status = CPXgetrngval((problem)->solver->backend.cplex.env, (problem)->backend.cplex, &rngval, (i), (i)); \
      CPLEX_FATALERROR((problem)->solver, status);                                                              \
      (ub) = rhs + rngval;                                                                                      \
    }                                                                                                           \
    else {                                                                                                      \
      (ub) = rhs;                                                                                               \
    }                                                                                                           \
  }                                                                                                             \
} while(0)
#define CPLEX_PROBLEM_SET_ROW_BOUNDS(problem, i, bounds_type, lb, ub)                                        \
do {                                                                                                         \
  int status;                                                                                                \
  char sense;                                                                                                \
  double rhs;                                                                                                \
  switch ((bounds_type)) {                                                                                   \
    case LPSOLVER_ROW_BOUNDS_TYPE_FX:                                                                        \
      sense = 'E';                                                                                           \
      rhs =  (lb);                                                                                           \
      break;                                                                                                 \
    case LPSOLVER_ROW_BOUNDS_TYPE_LB:                                                                        \
      sense = 'G';                                                                                           \
      rhs =  (lb);                                                                                           \
      break;                                                                                                 \
    case LPSOLVER_ROW_BOUNDS_TYPE_UB:                                                                        \
      sense = 'L';                                                                                           \
      rhs =  (ub);                                                                                           \
      break;                                                                                                 \
    case LPSOLVER_ROW_BOUNDS_TYPE_DB:                                                                        \
      sense = 'R';                                                                                           \
      rhs =  (lb);                                                                                           \
      break;                                                                                                 \
  }                                                                                                          \
  status = CPXchgsense((problem)->solver->backend.cplex.env, (problem)->backend.cplex, 1, &(i), &sense);     \
  CPLEX_FATALERROR((problem)->solver, status);                                                               \
  status = CPXchgrhs((problem)->solver->backend.cplex.env, (problem)->backend.cplex, 1, &(i), &rhs);         \
  CPLEX_FATALERROR((problem)->solver, status);                                                               \
  if (sense == 'R') {                                                                                        \
    double rngval = (ub) - (lb);                                                                             \
    status = CPXchgrngval((problem)->solver->backend.cplex.env, (problem)->backend.cplex, 1, &(i), &rngval); \
    CPLEX_FATALERROR((problem)->solver, status);                                                             \
  }                                                                                                          \
} while(0)
#define CPLEX_PROBLEM_GET_ROW_NAME(problem, i, name)                                                         \
do {                                                                                                         \
  int surplus = 0;                                                                                                              \
  int status = 0;                                                                                                               \
  status = CPXgetrowname((problem)->solver->backend.cplex.env, (problem)->backend.cplex, NULL, NULL, 0, &surplus, (i), (i));        \
  int sz = -surplus;                                                                                                            \
  char *namestore = malloc((size_t) sz);                                                                                            \
  status = CPXgetrowname((problem)->solver->backend.cplex.env, (problem)->backend.cplex, &name, namestore, sz, &surplus, (i), (i)); \
  CPLEX_FATALERROR((problem)->solver, status);                                                           \
} while(0)
#define CPLEX_PROBLEM_SET_ROW_NAME(problem, i, name)                                                         \
do {                                                                                                         \
  int status = CPXchgname((problem)->solver->backend.cplex.env, (problem)->backend.cplex, 'r', (i), (name)); \
  CPLEX_FATALERROR((problem)->solver, status);                                                               \
} while(0)
#define CPLEX_PROBLEM_GET_ROW_IDX(problem, name, i)                                                          \
do {                                                                                                         \
  int status = CPXgetrowindex((problem)->solver->backend.cplex.env, (problem)->backend.cplex, (name), &(i)); \
  if (status != 0) {                                                                                         \
    (i) = -1;                                                                                                \
  }                                                                                                          \
} while(0)
#define CPLEX_PROBLEM_ADD_ROW(problem, i)                                                                             \
do {                                                                                                                  \
  CPLEX_PROBLEM_GET_NUM_ROWS(problem, i);                                                                             \
  int status = CPXnewrows((problem)->solver->backend.cplex.env, (problem)->backend.cplex, 1, NULL, NULL, NULL, NULL); \
  CPLEX_FATALERROR((problem)->solver, status);                                                                        \
} while(0)
#define CPLEX_PROBLEM_DEL_ROW(problem, i)                                                            \
do {                                                                                                 \
  int status = CPXdelrows((problem)->solver->backend.cplex.env, (problem)->backend.cplex, (i), (i)); \
  CPLEX_FATALERROR((problem)->solver, status);                                                       \
} while(0)
#define CPLEX_PROBLEM_GET_OBJ_DIR(problem, dir) (dir) = CPXgetobjsen((problem)->solver->backend.cplex.env, (problem)->backend.cplex) == CPX_MIN ? LPSOLVER_OBJ_DIR_MIN : LPSOLVER_OBJ_DIR_MAX
#define CPLEX_PROBLEM_SET_OBJ_DIR(problem, dir) CPXchgobjsen((problem)->solver->backend.cplex.env, (problem)->backend.cplex, (dir) == LPSOLVER_OBJ_DIR_MIN ? CPX_MIN : CPX_MAX)
#define CPLEX_PROBLEM_GET_OBJ_COEFF(problem, j, coeff)                                                    \
do {                                                                                                      \
  int status;                                                                                             \
  status = CPXgetobj((problem)->solver->backend.cplex.env, (problem)->backend.cplex, &(coeff), (j), (j)); \
  CPLEX_FATALERROR((problem)->solver, status);                                                            \
} while(0)
#define CPLEX_PROBLEM_SET_OBJ_COEFF(problem, j, coeff)                                                   \
do {                                                                                                     \
  int status;                                                                                            \
  status = CPXchgobj((problem)->solver->backend.cplex.env, (problem)->backend.cplex, 1, &(j), &(coeff)); \
  CPLEX_FATALERROR((problem)->solver, status);                                                           \
} while(0)
#define CPLEX_PROBLEM_GET_OBJ_OFFSET(problem, offset)                                                  \
do {                                                                                                   \
  int status;                                                                                          \
  status = CPXgetobjoffset((problem)->solver->backend.cplex.env, (problem)->backend.cplex, &(offset)); \
  CPLEX_FATALERROR((problem)->solver, status);                                                         \
} while(0)
#define CPLEX_PROBLEM_SET_OBJ_OFFSET(problem, offset)                                                 \
do {                                                                                                  \
  int status;                                                                                         \
  status = CPXchgobjoffset((problem)->solver->backend.cplex.env, (problem)->backend.cplex, (offset)); \
  CPLEX_FATALERROR((problem)->solver, status);                                                        \
} while(0)
#define CPLEX_PROBLEM_GET_OBJ_COEFF_QUAD(problem, j1, j2, coeff)                                               \
do {                                                                                                           \
  int status;                                                                                                  \
  status = CPXgetqpcoef((problem)->solver->backend.cplex.env, (problem)->backend.cplex, (j1), (j2), &(coeff)); \
  CPLEX_FATALERROR((problem)->solver, status);                                                                 \
} while(0)
#define CPLEX_PROBLEM_SET_OBJ_COEFF_QUAD(problem, j1, j2, coeff)                                              \
do {                                                                                                          \
  int status;                                                                                                 \
  status = CPXchgqpcoef((problem)->solver->backend.cplex.env, (problem)->backend.cplex, (j1), (j2), (coeff)); \
  CPLEX_FATALERROR((problem)->solver, status);                                                                \
} while(0)
#define CPLEX_PROBLEM_OPTIMIZE(problem, res)                                                                                                      \
do {                                                                                                                                              \
  int solntype;                                                                                                                                   \
  int status;                                                                                                                                     \
  int num_quad = CPXgetnumquad((problem)->solver->backend.cplex.env, (problem)->backend.cplex);                                                   \
  int num_int = CPXgetnumint((problem)->solver->backend.cplex.env, (problem)->backend.cplex);                                                     \
  int num_bin = CPXgetnumbin((problem)->solver->backend.cplex.env, (problem)->backend.cplex);                                                     \
  if (num_quad > 0) {                                                                                                                             \
    status = CPXchgprobtype((problem)->solver->backend.cplex.env, (problem)->backend.cplex, (num_int + num_bin > 0) ? CPXPROB_MIQP : CPXPROB_QP); \
    CPLEX_FATALERROR((problem)->solver, status);                                                                                                  \
    status = CPXqpopt((problem)->solver->backend.cplex.env, (problem)->backend.cplex);                                                            \
    CPLEX_FATALERROR((problem)->solver, status);                                                                                                  \
  }                                                                                                                                               \
  else if (num_int + num_bin > 0) {                                                                                                               \
    status = CPXchgprobtype((problem)->solver->backend.cplex.env, (problem)->backend.cplex, CPXPROB_MILP);                                        \
    CPLEX_FATALERROR((problem)->solver, status);                                                                                                  \
    status = CPXmipopt((problem)->solver->backend.cplex.env, (problem)->backend.cplex);                                                           \
    CPLEX_FATALERROR((problem)->solver, status);                                                                                                  \
  }                                                                                                                                               \
  else {                                                                                                                                          \
    status = CPXchgprobtype((problem)->solver->backend.cplex.env, (problem)->backend.cplex, CPXPROB_LP);                                          \
    status = CPXlpopt((problem)->solver->backend.cplex.env, (problem)->backend.cplex);                                                            \
    CPLEX_FATALERROR((problem)->solver, status);                                                                                                  \
  }                                                                                                                                               \
  status = CPXsolninfo((problem)->solver->backend.cplex.env, (problem)->backend.cplex, NULL, &solntype, NULL, NULL);                              \
  CPLEX_FATALERROR((problem)->solver, status);                                                                                                    \
  (res) = solntype != CPX_NO_SOLN;                                                                                                                \
} while(0)
#define CPLEX_PROBLEM_GET_OBJ_VALUE(problem, value)                                                \
do {                                                                                               \
  int status;                                                                                      \
  status = CPXgetobjval((problem)->solver->backend.cplex.env, (problem)->backend.cplex, &(value)); \
  CPLEX_FATALERROR((problem)->solver, status);                                                     \
} while(0)
#define CPLEX_PROBLEM_GET_COL_VALUE(problem, j, value)                                                  \
do {                                                                                                    \
  int status;                                                                                           \
  status = CPXgetx((problem)->solver->backend.cplex.env, (problem)->backend.cplex, &(value), (j), (j)); \
  CPLEX_FATALERROR((problem)->solver, status);                                                          \
} while(0)
#else
#define CPLEX_IS_AVAILABLE false
#define CPLEX_SOLVER_DEF
#define CPLEX_SOLVER_NEW(solver) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_SOLVER_DESTROY(solver) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_SOLVER_SET_NUM_THREADS(solver, num) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_SOLVER_SET_TIME_LIMIT(solver, limit) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_SOLVER_SET_INTEGRALITY_TOLERANCE(solver, tol)  assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_DEF
#define CPLEX_PROBLEM_NEW(problem, name, num_rows, num_cols) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_NEW_FROM_FILE(problem, filename) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_DESTROY(problem) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_WRITE(problem, filename) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_GET_NUM_ROWS(problem, num_rows) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_GET_NUM_COLS(problem, num_cols) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_GET_COL_TYPE(problem, j, col_type) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_SET_COL_TYPE(problem, j, col_type) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_GET_COL_BOUNDS_TYPE(problem, j, col_bounds_type) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_GET_COL_LB(problem, j, lb) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_GET_COL_UB(problem, j, lb) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_SET_COL_BOUNDS(problem, j, bounds_type, lb, ub) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_GET_COL_NAME(problem, j, name) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_SET_COL_NAME(problem, j, name) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_GET_COL_IDX(problem, name, j) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_ADD_COL(problem, j) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_GET_ROW_COEFF(problem, i, j, coeff) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_SET_ROW_COEFF(problem, i, j, coeff) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_GET_ROW_BOUNDS_TYPE(problem, i, bounds_type) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_GET_ROW_LB(problem, i, lb) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_GET_ROW_UB(problem, i, ub) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_SET_ROW_BOUNDS(problem, i, bounds_type, lb, ub) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_GET_ROW_NAME(problem, i, name) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_SET_ROW_NAME(problem, i, name) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_GET_ROW_IDX(problem, name, i) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_ADD_ROW(problem, i) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_DEL_ROW(problem, i) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_GET_OBJ_DIR(problem, dir) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_SET_OBJ_DIR(problem, dir) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_GET_OBJ_COEFF(problem, j, coeff) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_SET_OBJ_COEFF(problem, j, coeff) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_GET_OBJ_OFFSET(problem, coeff) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_SET_OBJ_OFFSET(problem, coeff) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_GET_OBJ_COEFF_QUAD(problem, j1, j2, coeff) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_SET_OBJ_COEFF_QUAD(problem, j1, j2, coeff) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_OPTIMIZE(problem, res) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_GET_OBJ_VALUE(problem, value) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#define CPLEX_PROBLEM_GET_COL_VALUE(problem, j, value) assert(false); lpsolver_fatalerror(true, "CPLEX backend is not available")
#endif /* #ifdef CPLEX_FOUND */

#endif /* #ifndef CPLEX_MACRO_H_ */
