/*
 * Copyright (C) 2016 Vadim Alimguzhin
 *
 * This file is part of LPSOLVER open source library.
 *
 * LPSOLVER library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * LPSOLVER library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file lpsolver.h
 *
 * Public interface of LPSOLVER library.
 */

#ifndef LPSOLVER_H_
#define LPSOLVER_H_

#include <stdbool.h>

/**
 * Supported backends.
 */
typedef enum LPSolverBackendType {
  LPSOLVER_BACKEND_TYPE_GLPK, ///< GNU Linear Programming Kit
  LPSOLVER_BACKEND_TYPE_LP_SOLVE, ///< LP_SOLVE
  LPSOLVER_BACKEND_TYPE_CPLEX ///< IBM ILOG CPLEX
} LPSolverBackendType;

/**
 * Column types.
 */
typedef enum LPSolverColType {
  LPSOLVER_COL_TYPE_C, ///< continuous
  LPSOLVER_COL_TYPE_I, ///< integer
  LPSOLVER_COL_TYPE_B  ///< binary (integer with range 0 .. 1)
} LPSolverColType;

/**
 * Column bounds types.
 */
typedef enum LPSolverColBoundsType {
  LPSOLVER_COL_BOUNDS_TYPE_FR, ///< free (-inf <= col <= inf)
  LPSOLVER_COL_BOUNDS_TYPE_FX, ///< fixed (lb = col = ub)
  LPSOLVER_COL_BOUNDS_TYPE_LB, ///< lower bound (lb <= col <= inf)
  LPSOLVER_COL_BOUNDS_TYPE_UB, ///< upper bound (-inf <= col <= ub)
  LPSOLVER_COL_BOUNDS_TYPE_DB  ///< double bounds (lb <= col <= ub)
} LPSolverColBoundsType;

/**
 * Row bounds types.
 */
typedef enum LPSolverRowBoundsType {
  LPSOLVER_ROW_BOUNDS_TYPE_FX, ///< fixed ( lb = row = ub)
  LPSOLVER_ROW_BOUNDS_TYPE_LB, ///< lower bound (lb <= row <= inf)
  LPSOLVER_ROW_BOUNDS_TYPE_UB, ///< upper bound (-inf <= row <= ub)
  LPSOLVER_ROW_BOUNDS_TYPE_DB  ///< double bounds (lb <= row <= ub)
} LPSolverRowBoundsType;

/**
 * Objective function directions.
 */
typedef enum LPSolverObjDir {
  LPSOLVER_OBJ_DIR_MIN, ///< minimization
  LPSOLVER_OBJ_DIR_MAX  ///< maximization
} LPSolverObjDir;

/**
 * Solver object
 */
typedef struct LPSolver LPSolver;

/**
 * Problem object
 */
typedef struct LPSolverProblem LPSolverProblem;

/**
 * Check whether backend is available.
 *
 * \param backend_type
 *
 * \return true if backend is available, false otherwise
 */
extern bool
lpsolver_has_backend(LPSolverBackendType backend_type);

/**
 * Create solver object.
 *
 * \param backend_type
 *
 * \return solver
 */
extern LPSolver *
lpsolver_new(LPSolverBackendType backend_type);

/**
 * Destroy solver object.
 *
 * \param solver
 */
extern void
lpsolver_destroy(LPSolver **solver_ptr);

/**
 * Set number of threads that solver can invoke.
 *
 * It is supported only by CPLEX backend.
 * By default CPLEX automatically decides how many threads to invoke.
 * If you want to be guaranteed that it uses only 1 thread you have to use this routine to set number of threads to 1.
 *
 * \param num number of threads
 */
extern void
lpsolver_set_num_threads(LPSolver *solver, int num);

/**
 * Set time limit in seconds for solution time.
 *
 * It is supported only by CPLEX backend.
 * By default CPLEX uses 1e+75 as time limit.
 *
 * \param limit number of seconds
 */
extern void
lpsolver_set_time_limit(LPSolver *solver, double limit);

/**
 * Set tolerance used to check integrality constraints.
 *
 * \param integrality tolerance value
 */
extern void
lpsolver_set_integrality_tolerance(LPSolver *solver, double tol);

/**
 * Create problem object.
 *
 * All constraints are initialized to be of the form 0 col_0 + ... + 0 col_{m-1} = 0.
 * All variables are initialized to have bounds >= 0.
 *
 * \param problem
 * \param name
 * \param num_rows number of rows (constraints)
 * \param num_cols number of cols (variables)
 *
 * \return problem
 */
extern LPSolverProblem *
lpsolver_problem_new(LPSolver *solver, char const *name, int num_rows, int num_cols);

/**
 * Create problem object reading it from LP file.
 *
 * \param problem
 * \param filename
 *
 * \return problem
 */
extern LPSolverProblem *
lpsolver_problem_new_from_file(LPSolver *solver, char const *filename);

/**
 * Destroy problem object.
 *
 * \param problem
 */
extern void
lpsolver_problem_destroy(LPSolverProblem **problem);

/**
 * Write problem to the file.
 *
 * \param problem
 * \param filename
 */
extern void
lpsolver_problem_write(LPSolverProblem const *problem, char const *filename);

/**
 * Get number of rows in the problem.
 *
 * \param problem
 *
 * \return number of rows in problem
 */
extern int
lpsolver_problem_get_num_rows(LPSolverProblem const *problem);

/**
 * Get number of columns in the problem.
 *
 * \param problem
 *
 * \return number of columns in problem
 */
extern int
lpsolver_problem_get_num_cols(LPSolverProblem const * problem);

/**
 * Get type of problem's column.
 *
 * \param problem
 * \param j column index
 *
 * \return j-th column type
 */
extern LPSolverColType
lpsolver_problem_get_col_type(LPSolverProblem const *problem, int j);

/**
 * Set type of problem's column.
 *
 * \param problem
 * \param j column index
 * \param col_type column type
 */
extern void
lpsolver_problem_set_col_type(LPSolverProblem *problem, int j, LPSolverColType col_type);

/**
 * Set type of problem's column.
 *
 * \param problem
 * \param j column index
 *
 * \return j-th column bounds type
 */
extern LPSolverColBoundsType
lpsolver_problem_get_col_bounds_type(LPSolverProblem const *problem, int j);

/**
 * Get lower bound of problem's column.
 *
 * \param problem
 * \param j column index
 *
 * \return lower bound of j-th column (if bounds type is FR/UB then -DBL_MAX)
 */
extern double
lpsolver_problem_get_col_lb(LPSolverProblem const *problem, int j);

/**
 * Get upper bound of problem's column.
 *
 * \param problem
 * \param j column index
 *
 * \return upper bound of j-th column (if bounds type is FR/LB then DBL_MAX)
 */
extern double
lpsolver_problem_get_col_ub(LPSolverProblem const *problem, int j);

/**
 * Set bounds of problem's column.
 *
 * \param problem
 * \param j column index
 * \param bounds_type column bounds type
 * \param lb lower bound (ignored if bounds type is FR/UB)
 * \param ub upper bound (ignored if bounds type is FR/FX/LB)
 */
extern void
lpsolver_problem_set_col_bounds(LPSolverProblem *problem, int j, LPSolverColBoundsType bounds_type, double lb, double ub);

/**
 * Get column name.
 *
 * It is user responsability to free the result.
 *
 * \param problem
 * \param j column index
 *
 * \return name of j-th column
 */
extern char *
lpsolver_problem_get_col_name(LPSolverProblem *problem, int j);

/**
 * Set column name.
 *
 * \param problem
 * \param j column index
 * \param name_fmt format string for column name
 */
extern void
lpsolver_problem_set_col_name(LPSolverProblem *problem, int j, char const *name_fmt, ...);

/**
 * Get column index by name.
 *
 * \param problem
 * \param name_fmt format string for column name
 *
 * \return column index if column with given name exists, -1 otherwise
 */
extern int
lpsolver_problem_get_col_idx(LPSolverProblem const *problem, char const *name_fmt, ...);

/**
 * Add new column to the problem.
 *
 * If name_fmt is NULL, then name is assigned by the backend.
 *
 * \param problem
 * \param col_type column type
 * \param bounds_type column bounds type
 * \param lb lower bound (ignored if bounds type is FR/UB)
 * \param ub upper bound (ignored if bounds type is FR/FX/LB)
 * \param name_fmt format string for column name
 *
 * \return index of new column
 */
extern int
lpsolver_problem_add_col(LPSolverProblem *problem, LPSolverColType col_type, LPSolverColBoundsType bounds_type, double lb, double ub, char const *name_fmt, ...);

/**
 * Get coefficient of problem's row.
 *
 * \param problem
 * \param i row coefficient
 * \param j column coefficient
 *
 * \return coefficient of j-th column for i-th row
 */
extern double
lpsolver_problem_get_row_coeff(LPSolverProblem const *problem, int i, int j);

/**
 * Set coefficient of problem's row coefficient.
 *
 * \param problem
 * \param i row index
 * \param j column index
 * \param coeff coefficient value
 */
extern void
lpsolver_problem_set_row_coeff(LPSolverProblem *problem, int i, int j, double coeff);

/**
 * Get bounds type of problem's row.
 *
 * \param problem
 * \param i row index
 *
 * \return bounds type of i-th row.
 */
extern LPSolverRowBoundsType
lpsolver_problem_get_row_bounds_type(LPSolverProblem const * problem, int i);

/**
 * Get lower bound of problem's row.
 *
 * \param problem
 * \param i row index
 *
 * \return lower bound of i-th row (if bounds_type is UB then -DBL_MAX)
 */
extern double
lpsolver_problem_get_row_lb(LPSolverProblem const *problem, int i);

/**
 * Get upper bound of problem's row.
 *
 * \param problem
 * \param i row index
 *
 * \return upper bound of i-th row (if bounds type is LB then DBL_MAX)
 */
extern double
lpsolver_problem_get_row_ub(LPSolverProblem const *problem, int i);

/**
 * Set bounds of problem's row.
 *
 * \param problem
 * \param i row index
 * \param bounds_type row bounds type
 * \param lb lower bound (ignored if bounds type is UB)
 * \param ub upper bound (ignored if bounds type is FX/LB)
 */
extern void
lpsolver_problem_set_row_bounds(LPSolverProblem *problem, int i, LPSolverRowBoundsType bounds_type, double lb, double ub);

/**
 * Get row name.
 *
 * It is user responsability to free the result.
 *
 * \param problem
 * \param i row index
 *
 * \return name of i-th row
 */
extern char *
lpsolver_problem_get_row_name(LPSolverProblem const * problem, int i);

/**
 * Set row name.
 *
 * \param problem
 * \param i row index
 * \param name_fmt format string for row name
 */
extern void
lpsolver_problem_set_row_name(LPSolverProblem *problem, int i, char const *name_fmt, ...);

/**
 * Get row index by name.
 *
 * \param problem
 * \param name_fmt format string for row name
 *
 * \return row index if row with given name exists, -1 otherwise
 */
extern int
lpsolver_problem_get_row_idx(LPSolverProblem const *problem, char const *name_fmt, ...);

/**
 * Add new row to the problem.
 *
 * If name_fmt is NULL, then name is assigned by the backend.
 *
 * \param problem
 * \param bounds_type row bounds type
 * \param lb lower bound (ignored if bounds type is UB)
 * \param ub upper bound (ignored if bounds type is FX/LB)
 * \param name_fmt format string for row name
 *
 * \return index of new row
 */
extern int
lpsolver_problem_add_row(LPSolverProblem *problem, LPSolverRowBoundsType bounds_type, double lb, double ub, char const *name_fmt, ...);

/**
 * Delete row from the problem.
 *
 * \param problem
 * \param i row index
 *
 */
extern void
lpsolver_problem_del_row(LPSolverProblem *problem, int i);

/**
 * Get problem's objective function direction.
 *
 * \param problem
 *
 * \return direction of problem's objective function
 */
extern LPSolverObjDir
lpsolver_problem_get_obj_dir(LPSolverProblem const *problem);

/**
 * Set direction of problem's objective function.
 *
 * \param problem
 * \param dir direction of problem's objective function
 */
extern void
lpsolver_problem_set_obj_dir(LPSolverProblem *problem, LPSolverObjDir dir);

/**
 * Get coefficient of problem's objective function.
 *
 * \param problem
 * \param j column index
 *
 * \return coefficient of j-th column
 */
extern double
lpsolver_problem_get_obj_coeff(LPSolverProblem const *problem, int j);

/**
 * Set coefficient of problem's objective function.
 *
 * \param problem
 * \param j column index
 * \param coeff coefficient value
 */
extern void
lpsolver_problem_set_obj_coeff(LPSolverProblem *problem, int j, double coeff);

/**
 * Get offset of problem's objective function.
 *
 * \param problem
 *
 * \return offset value
 */
extern double
lpsolver_problem_get_obj_offset(LPSolverProblem const *problem);

/**
 * Set offset of problem's objective function.
 *
 * \param problem
 * \param offset offset value
 */
extern void
lpsolver_problem_set_obj_offset(LPSolverProblem *problem, double offset);

/**
 * Get coefficient of problem's quadratic objective function.
 *
 * \param problem
 * \param j1 first column index
 * \param j2 second column index
 *
 * \return coefficient of the variable pair (j1, j2)
 */
extern double
lpsolver_problem_get_obj_coeff_quad(LPSolverProblem const *problem, int j1, int j2);

/**
 * Set coefficient of problem's quadratic objective function.
 *
 * \param problem
 * \param j1 first column index
 * \param j2 second column index
 * \param coeff coefficient value
 *
 * Coefficient is set for variable pairs (j1, j2) and (j2, j1).
 */
extern void
lpsolver_problem_set_obj_coeff_quad(LPSolverProblem *problem, int j1, int j2, double coeff);


/**
 * Add guard to problem's row.
 *
 * \param problem
 * \param row_idx row index
 * \param guard_col_idx guard column index
 * \param negated specifies whether guard is negated or not
 *
 * \return 0 if guard was added.
 */
extern int lpsolver_problem_add_guard(LPSolverProblem *problem, int row_idx,
                                      int guard_col_idx, bool negated);

/**
 * Solve problem.
 *
 * \param problem
 *
 * \return true if success, false if fail
 */
extern bool
lpsolver_problem_optimize(LPSolverProblem *problem);

/**
 * Get value of problem's objective function.
 *
 * \param problem
 *
 * \return value of problem's objective function
 */
extern double
lpsolver_problem_get_obj_value(LPSolverProblem const *problem);

/**
 * Get column value of problem's solution.
 *
 * \param problem
 * \param j column index
 *
 * \return j-th column value of problem's solution
 */
extern double
lpsolver_problem_get_col_value(LPSolverProblem const *problem, int j);

#endif /* #ifndef LPSOLVER_H_ */
